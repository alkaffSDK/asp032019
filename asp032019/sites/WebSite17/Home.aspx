﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBoxASP" runat="server" Text="ASP"></asp:TextBox>
        <asp:Button ID="ButtonASP" runat="server" Text="Button ASP" OnClick="ButtonASP_Click" />
        <br />
        <input id="TextHTML" type="text" value="HTML" runat="server"  />
        <input id="ButtonHTML" type="submit" runat="server"  value="button HTML" onclick="ButtonASP_Click"  />
        <br />
        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="CountryName" DataValueField="CountryId" AutoPostBack="True" OnDataBound="DropDownList1_DataBound" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CountriesConnectionString %>" SelectCommand="SELECT [CountryName], [CountryId] FROM [Country]"></asp:SqlDataSource>

        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <br />
        <asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>
    </div>
    </form>
</body>
</html>
