﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pageNavigation_Source : System.Web.UI.Page
{

    public string MyText { get { return TextBox1.Text; }  }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonRedirect_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/pageNavigation/Target.aspx?f1="+Server.HtmlEncode(TextBox1.Text)+"&f2="+ Server.HtmlEncode(TextBox2.Text));
    }

    protected void LinkButtonRedirect_Click(object sender, EventArgs e)
    {
        Response.Redirect("http://sdkjordan.com/");
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if(sender is LinkButton)
        {
            LinkButton linkBtn =(LinkButton) sender;
            switch (linkBtn.ClientID)
            {
                case "LinkButtonTransfer":
                    Server.Transfer("~/pageNavigation/Target.aspx");
                    break;
                case "LinkButtonExecute":
                    Server.Execute("~/pageNavigation/Target.aspx");
                    break;
                case "LinkButtonWindowOpen":
                    Response.Write("<script>");
                    Response.Write("window.open('http://google.com/','_blank');");
                    Response.Write("</script>");
                    Response.End();
                    break;
            }
        }
        
       
    }

    protected void LinkButtonCrossPostBack_Click(object sender, EventArgs e)
    {
        //Response.Redirect("http://sdkjordan.com/");

    }

    protected void LinkButton1_Click1(object sender, EventArgs e)
    {

    }
}