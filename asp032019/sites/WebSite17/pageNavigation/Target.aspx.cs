﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pageNavigation_Target : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(! IsPostBack)
        {
            TextBox1.Text = "==================== QueryString ====================\n";
            TextBox1.Text += "Text Box1:" +Server.HtmlDecode(Request.QueryString["f1"] + ", TextBox2:" + Server.HtmlDecode(Request.QueryString["f2"]) + "\n");
            TextBox1.Text += "\n==================== Form collection ====================\n";
            foreach (string k in Request.Form.AllKeys)
            {
               // if (!k.StartsWith("__"))
                    TextBox1.Text += string.Format("{0,-20}:{1}\n", k, Request.Form[k]);
            }

            if (PreviousPage != null)
            {
                TextBox1.Text += "\n==================== PreviousPage ====================\n";
                TextBox text = PreviousPage.FindControl("TextBox1") as TextBox;
                if (text != null)
                    TextBox1.Text += string.Format("{0,-20} :{1}\n", "TextBox.Text", text.Text);
               


                // you need to add this line to the top of the target page 
                // <%@ PreviousPageType VirtualPath="~/pageNavigation/Source.aspx" %>

                TextBox1.Text += "\n==================== Public Properity ====================\n";
                TextBox1.Text += string.Format("{0,-20} :{1}\n", "MyText", PreviousPage.MyText );

                Label1.Text = PreviousPage.IsCrossPagePostBack.ToString();
            }
          
           
        }
    }

    protected void LinkButtonCrossPostBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("http://google.com/");
    }
}