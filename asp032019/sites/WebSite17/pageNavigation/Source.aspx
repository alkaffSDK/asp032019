﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Source.aspx.cs" Inherits="pageNavigation_Source" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            background-color: #C0C0C0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table align="center" class="auto-style1" dir="ltr">
            <tr>
                <td>

        <asp:TextBox ID="TextBox1" runat="server">
          
        </asp:TextBox>
                </td>
                <td>
         <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
        <asp:HyperLink ID="HyperLinkInternal" runat="server" NavigateUrl="~/pageNavigation/Target.aspx">HyperLink Internal</asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="HyperLinkExternal" runat="server" NavigateUrl="http://sdkjordan.com/">HyperLink External</asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="HyperLinkNewTable" runat="server" NavigateUrl="http://sdkjordan.com/" Target="_blank">HyperLink External</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonRedirect" runat="server" Text="Response Redirect Internal" OnClick="ButtonRedirect_Click" />
                </td>
                <td>
                    <asp:LinkButton ID="LinkButtonRedirect" runat="server" OnClick="LinkButtonRedirect_Click">Response Redirect External</asp:LinkButton>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:LinkButton ID="LinkButtonTransfer" runat="server" OnClick="LinkButton1_Click">Server transfer</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:LinkButton ID="LinkButtonExecute" runat="server" OnClick="LinkButton1_Click">Server execute</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="3"> <asp:LinkButton ID="LinkButtonCrossPostBack" runat="server"  PostBackUrl="~/pageNavigation/Target.aspx" OnClick="LinkButtonCrossPostBack_Click">Cross Page post back</asp:LinkButton></td>
          
            </tr>
            <tr>
                <td colspan="3"> <asp:LinkButton ID="LinkButtonWindowOpen" runat="server" OnClick="LinkButton1_Click">Window open</asp:LinkButton>
                    &nbsp;</td>
          
            </tr>
        </table>
        <br />
         &nbsp;
         <br />
        
    </div>
    </form>
</body>
</html>
