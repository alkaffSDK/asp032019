﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for MyWebService
/// </summary>
[WebService(Namespace = "http://sdkjordan.com/"  )]
[WebServiceBinding(ConformsTo = WsiProfiles.None)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class MyWebService : System.Web.Services.WebService
{

    public MyWebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod (Description ="This is a hello World method" , MessageName = "HelloWorld")]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod(Description = "This is a hello by name", MessageName = "HelloUser")]
    public string HelloWorld(string name)
    {
        return "Hello "+ name;
    }


    public string Test()
    {
        return "Test";
    }

    [WebMethod]
    public bool Login(string uname, string pass)
    {
        // Do the connection and check user name and password
        return true;
    }

}
