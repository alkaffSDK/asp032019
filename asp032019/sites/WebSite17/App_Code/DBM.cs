﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DBM
/// </summary>
public class DBM
{
    public DBM()
    {

    }

    public int AddUser(String fname, string lname, string login, DateTime bdate, bool gender, string email, string phone, string pass, int cointryID)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["asp032019ConnectionString"].ConnectionString))
        using (SqlCommand cmd = new SqlCommand("AddUser", con))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FName", fname);
            cmd.Parameters.AddWithValue("@LName", lname);
            cmd.Parameters.AddWithValue("@Login", login);
            cmd.Parameters.AddWithValue("@BirthDay", bdate);
            cmd.Parameters.AddWithValue("@Gender", gender);
            cmd.Parameters.AddWithValue("@Email", email);
            cmd.Parameters.AddWithValue("@Phone", phone);
            cmd.Parameters.AddWithValue("@Password", pass);
            cmd.Parameters.AddWithValue("@CountryID", cointryID);

            var returnParameter = cmd.Parameters.Add("@ReturnVal", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;
           
            cmd.ExecuteNonQuery();
            int result;
            int.TryParse(returnParameter.Value.ToString(), out result);

            switch (result)
            {
                case -5:
                    throw new ArgumentException("email already  exist.");
                case -4:
                    throw new ArgumentException("Phone number is  already exist.");
                case 0:
                    throw new Exception("Something wrong has happended.");

                default:
                    return result;

            }
        }
    }
}

