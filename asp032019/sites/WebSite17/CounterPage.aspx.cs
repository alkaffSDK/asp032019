﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CounterPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(! IsPostBack)
        {
            TextBox1.Text = "0";
            if (null != Request.Cookies["c"])
            {
                TextBox2.Text = Request.Cookies["c"].Value;
            }
            else TextBox2.Text = "0";
            
            TextBox3.Text = "0";
        }
    }

   
    protected void Button1_Click(object sender, EventArgs e)
    {
        int cv;
        int.TryParse(Label1.Text, out cv);
        Label1.Text = (++cv).ToString();

        int counter = 0;
        if(null != ViewState["counter"])
        {
            int.TryParse(ViewState["counter"].ToString(), out counter);
        }
        ViewState["counter"] = ++counter;
        TextBox1.Text = counter++.ToString();



        int c = 0;
        if(null != Request.Cookies["c"])
        {
            int.TryParse(Request.Cookies["c"].Value, out c);
        }
        c++;
        TextBox2.Text = c.ToString();
        Response.Cookies["c"].Value = c.ToString();
        Response.Cookies["c"].Expires = DateTime.Now.AddHours(1);

    }
}