﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            DropDownList1.SelectedIndex = 0;
            DropDownList2.Items.Insert(0, new ListItem("Please select a name", "0"));
        }
    }

    protected void ButtonASP_Click(object sender, EventArgs e)
    {
        //TextBoxASP.Text = "Changed !";
        //TextHTML.Value = "Changed !";
        //ButtonHTML.Value = "Fooo";

        DropDownList2.Items.Add(TextBoxASP.Text);

        
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label1.Text = DropDownList1.SelectedItem.Value + ":" + DropDownList1.SelectedItem.Text;
    }

    protected void DropDownList1_DataBound(object sender, EventArgs e)
    {
        DropDownList1.Items.Insert(0, new ListItem("Please select a country", "0"));
        DropDownList1.SelectedIndex = 0;
    }
}