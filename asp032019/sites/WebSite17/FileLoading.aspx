﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileLoading.aspx.cs" Inherits="FileLoading" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="True" />
        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem Enabled="true"  Text="Save to server"></asp:ListItem>
            <asp:ListItem Enabled="true"  Text="Save to Database"></asp:ListItem>

        </asp:RadioButtonList>
        <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
        <br />
        <br />
        <asp:DropDownList ID="DropDownList1" runat="server" Width="30%">
        </asp:DropDownList>
    </div>
    </form>
</body>
</html>
