﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class FileLoading : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadFiles(DropDownList1);
    }

    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        if(FileUpload1.HasFiles)
        {
            foreach(var file in FileUpload1.PostedFiles)
            {
                file.SaveAs(Server.MapPath("~/Files/") + file.FileName);
                
            }
        }
    }

    private void LoadFiles(DropDownList ddl)
    {
        ddl.Items.Add("-- Files --");

        DirectoryInfo df = new DirectoryInfo(Server.MapPath("~/Files/"));
        foreach (FileInfo file in df.GetFiles())
            ddl.Items.Add(file.Name);


        foreach (DirectoryInfo dire in df.GetDirectories())
            LoadFiles(ddl, dire);

   }



    private void LoadFiles(DropDownList ddl, DirectoryInfo df)
    {
      
        foreach (FileInfo file in df.GetFiles())
            ddl.Items.Add(string.Format("{0} -- {1}",df.Name,file.Name));

        foreach (DirectoryInfo dire in df.GetDirectories())
            LoadFiles(ddl, dire);
    }
}