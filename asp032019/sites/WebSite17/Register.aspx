﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterPage.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    Registration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContentTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContent1" Runat="Server">
    <h2 class="title">Registration Form</h2>
                        <div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">first name</label>
                                        <asp:TextBox ID="TextBoxFirst_name" runat="server" CssClass="input--style-4" placeholder="First name"></asp:TextBox>
                                        <%--   <input class="input--style-4" type="text" name="first_name">--%>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirstName" runat="server" ForeColor="Red" ErrorMessage="First name is required." ControlToValidate="TextBoxFirst_name" Display="Dynamic">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorName" runat="server" ForeColor="Red" ErrorMessage="Invalid name." ControlToValidate="TextBoxFirst_name" Display="Dynamic" ValidationExpression="[A-Za-z]{2,30}$">*</asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">last name</label>
                                        <asp:TextBox ID="TextBoxLastName" runat="server" CssClass="input--style-4" placeholder="Last name"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLastName" runat="server" ForeColor="Red" ErrorMessage="Last name is required." ControlToValidate="TextBoxLastName" Display="Dynamic">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                            </div>
                              <div class="row row-space">
                              <div class="input-group" style="width:100%">
                                        <label class="label">Login </label>
                                        <asp:TextBox ID="TextBoxLogin" runat="server" CssClass="input--style-4"  placeholder="Login"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ErrorMessage="Login is required." ControlToValidate="TextBoxLogin" Display="Dynamic">*</asp:RequiredFieldValidator>
                            </div>
                                  </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Password </label>
                                        <asp:TextBox ID="TextBoxPassword" runat="server" CssClass="input--style-4" TextMode="Password" placeholder="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ForeColor="Red" ErrorMessage="Password is required." ControlToValidate="TextBoxPassword" Display="Dynamic">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Re-password</label>
                                        <asp:TextBox ID="TextBoxRePassword" runat="server" CssClass="input--style-4" TextMode="Password" placeholder="Repasswrod"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorRepasswrod" runat="server" ForeColor="Red" ErrorMessage="Repasswrod is required." ControlToValidate="TextBoxRePassword" Display="Dynamic">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords are not matched." Text="*" ForeColor="Red" ControlToCompare="TextBoxPassword" ControlToValidate="TextBoxRePassword"  >*</asp:CompareValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Birthday</label>
                                        <div class="input-group-icon">
                                            <asp:TextBox ID="TextBoxBirthDate" runat="server" CssClass="input--style-4 js-datepicker"></asp:TextBox>
                                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBirthDate" runat="server" ForeColor="Red" ErrorMessage="Birth date is required." ControlToValidate="TextBoxBirthDate" Display="Dynamic">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Gender</label>
                                        <div class="p-t-10">


                                            <label class="radio-container m-r-45">
                                                Male
                                            <input id="RadioMale" type="radio" checked="true" runat="server" name="gender" />
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-container">
                                                Female
                                            <input id="RadioFemale" type="radio" runat="server" name="gender">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group">
                                            <label class="label">Email</label>
                                            <asp:TextBox ID="TextBoxEmail" runat="server" CssClass="input--style-4" placeholser="Email"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server" ForeColor="Red" ErrorMessage="Email is required." ControlToValidate="TextBoxEmail" Display="Dynamic">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server" ForeColor="Red" ErrorMessage="Invalid email." ControlToValidate="TextBoxEmail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group">
                                            <label class="label">Phone Number</label>
                                            <asp:TextBox ID="TextBoxPhone" runat="server" TextMode="Phone" CssClass="input--style-4" placeholser="Phone"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPhone" runat="server" ForeColor="Red" ErrorMessage="Phone is required." ControlToValidate="TextBoxPhone" Display="Dynamic">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group" style="width:100%">
                                    <label class="label">Country</label>

                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="CountryName" DataValueField="CountryId" OnDataBound="DropDownList1_DataBound">
                                            <asp:ListItem Text="Please select a subject" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Subject 1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Subject 2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Subject 3" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:asp032019ConnectionString %>" SelectCommand="SELECT [CountryName], [CountryId], [CountryAbbreiv] FROM [Country]"></asp:SqlDataSource>
                                        <%--       <select name="subject">
                                    <option disabled="disabled" selected="selected">Choose option</option>
                                    <option>Subject 1</option>
                                    <option>Subject 2</option>
                                    <option>Subject 3</option>
                                </select>--%>
                                        <div class="select-dropdown"></div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorSubject" runat="server" ForeColor="Red" ErrorMessage="Subject is required." ControlToValidate="DropDownList1" InitialValue="0" Display="Dynamic">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
                                </div>
                                
                           
                            </div>
                            <div class="row">
                                           <div class="col-2">
                                    <asp:Button ID="ButtonRegister" runat="server" Text="Register" CssClass="btn btn--radius-2 btn--blue" OnClick="Button1_Click" />
                                  </div>
                               <div class="col-2">
                                <asp:Button ID="ButtonHome" runat="server" Text="Home" CssClass="btn btn--radius-2 btn--green"  ValidationGroup="home" />
                                   </div>
                                <asp:Label ID="LabelResult" runat="server" Text="Label"></asp:Label>
                                </div>
                        </div>
</asp:Content>

