﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MyMasterPage : System.Web.UI.MasterPage
{

    public String MyProperty { get { return TextBoxName.Text; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(! IsPostBack)
        {
            if (Session["un"] == null)
            {
                //Server.Transfer("~/Registration.aspx");
                loginDiv.Visible = true;
                logoutDiv.Visible = false;
            }
            else
            {
                
                loginDiv.Visible = false;
                logoutDiv.Visible = true;
                Label1.Text = "Welcome " + Session["un"];
            }

            
        }

    }

    protected void Button_SignIn(object sender, EventArgs e)
    {
        //TextBox tb = ContentPlaceHolderContent1.FindControl("TextBox1") as TextBox;
        //if(null != tb)
        //{
        //    TextBoxName.Text = tb.Text;
        //}
    }

    protected void Button_Action(object sender, EventArgs e)
    {
        Control c = (Control)sender;
        switch(c.ClientID)
        {
            case "Signin":
                Singin(TextBoxName.Text,TextBoxPassword.Text);
                break;
            case "Logout":
                LogoutAction();
                break;

        }
    }

    private void Singin(string un, string pass)
    {
        if(! string.IsNullOrEmpty(un) &&
            !string.IsNullOrEmpty(pass) && 
            un.Equals(pass,StringComparison.CurrentCultureIgnoreCase))
        {
            Session["un"] = un; 
            Server.Transfer("~/DefaultWithMaster.aspx");

        }else
        {
            Server.Transfer("~/Registration.aspx");
        }
    }
    private void LogoutAction()
    {
        Session["un"] = null;
        Server.Transfer(Request.RawUrl);
    }
}
