USE [master]
GO
/****** Object:  Database [asp032019]    Script Date: 7/7/2019 12:06:17 PM ******/
CREATE DATABASE [asp032019]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'asp032019', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\asp032019.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'asp032019_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\asp032019_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [asp032019] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [asp032019].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [asp032019] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [asp032019] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [asp032019] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [asp032019] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [asp032019] SET ARITHABORT OFF 
GO
ALTER DATABASE [asp032019] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [asp032019] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [asp032019] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [asp032019] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [asp032019] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [asp032019] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [asp032019] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [asp032019] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [asp032019] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [asp032019] SET  DISABLE_BROKER 
GO
ALTER DATABASE [asp032019] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [asp032019] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [asp032019] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [asp032019] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [asp032019] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [asp032019] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [asp032019] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [asp032019] SET RECOVERY FULL 
GO
ALTER DATABASE [asp032019] SET  MULTI_USER 
GO
ALTER DATABASE [asp032019] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [asp032019] SET DB_CHAINING OFF 
GO
ALTER DATABASE [asp032019] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [asp032019] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [asp032019] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'asp032019', N'ON'
GO
USE [asp032019]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [int] NOT NULL,
	[CountryName] [nvarchar](100) NOT NULL,
	[CountryAbbreiv] [nvarchar](5) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CUSTOMERS]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUSTOMERS](
	[ID] [int] NOT NULL,
	[NAME] [varchar](20) NOT NULL,
	[AGE] [int] NOT NULL,
	[ADDRESS] [char](25) NULL,
	[SALARY] [decimal](18, 2) NULL,
	[DATE] [datetime2](7) NULL,
	[Activity] [bit] NOT NULL CONSTRAINT [DF_CUSTOMERS_Activity]  DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Emails]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Emails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Phones]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Phones](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Phone] [nvarchar](30) NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Phones] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(10,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NULL,
	[BirthDate] [date] NULL,
	[Gender] [bit] NULL,
	[CountryID] [int] NULL,
	[Login] [varchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime2](7) NULL,
	[ModifyDate] [datetime2](7) NULL,
	[Actvie] [bit] NULL CONSTRAINT [DF_Users_Actvie]  DEFAULT ((1)),
	[SupervisorID] [int] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vUsers]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vUsers]
AS
SELECT        dbo.Users.Name, dbo.Users.Login, Users_1.Name AS SupervisorName, dbo.Emails.Email
FROM            dbo.Emails INNER JOIN
                         dbo.Users ON dbo.Emails.UserID = dbo.Users.ID INNER JOIN
                         dbo.Users AS Users_1 ON dbo.Emails.UserID = Users_1.ID AND dbo.Users.SupervisorID = Users_1.ID

GO
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (2, N'Andorra', N'ad')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (3, N'United Arab Emirates', N'ae')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (4, N'Afghanistan', N'af')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (5, N'Antigua And Barbuda', N'ag')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (6, N'Anguilla', N'ai')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (7, N'Albania', N'al')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (8, N'Armenia', N'am')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (9, N'Netherlands Antilles', N'an')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (10, N'Angola', N'ao')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (11, N'Argentina', N'ar')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (12, N'Austria', N'at')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (13, N'Australia', N'au')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (14, N'Aruba', N'aw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (15, N'Azerbaijan', N'az')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (16, N'Bosnia And Herzegovina', N'ba')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (17, N'Barbados', N'bb')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (18, N'Bangladesh', N'bd')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (19, N'Belgium', N'be')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (20, N'Burkina Faso', N'bf')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (21, N'Bulgaria', N'bg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (22, N'Bahrain', N'bh')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (23, N'Burundi', N'bi')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (24, N'Benin', N'bj')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (25, N'Bermuda', N'bm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (26, N'Brunei Darussalam', N'bn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (27, N'Bolivia', N'bo')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (28, N'Brazil', N'br')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (29, N'Bahamas', N'bs')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (30, N'Bhutan', N'bt')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (31, N'Botswana', N'bw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (32, N'Belarus', N'by')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (33, N'Belize', N'bz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (34, N'Canada', N'ca')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (35, N'Cocos (Keeling) Islands', N'cc')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (36, N'Congo (Democratic Republic)', N'cd')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (37, N'Central African Republic', N'cf')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (38, N'Congo (Republic)', N'cg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (39, N'Switzerland', N'ch')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (40, N'Côte d''Ivoire', N'ci')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (41, N'Cook Islands', N'ck')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (42, N'Chile', N'cl')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (43, N'Cameroon', N'cm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (44, N'People''s Republic of China', N'cn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (45, N'Colombia', N'co')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (46, N'Costa Rica', N'cr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (47, N'Cuba', N'cu')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (48, N'Cape Verde', N'cv')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (49, N'Christmas Island', N'cx')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (50, N'Cyprus', N'cy')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (51, N'Czech Republic', N'cz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (52, N'Germany', N'de')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (53, N'Djibouti', N'dj')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (54, N'Denmark', N'dk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (55, N'Dominica', N'dm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (56, N'Dominican Republic', N'do')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (57, N'Algeria', N'dz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (58, N'Ecuador', N'ec')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (59, N'Estonia', N'ee')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (60, N'Egypt', N'eg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (61, N'Western Sahara', N'eh')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (62, N'Eritrea', N'er')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (63, N'Spain', N'es')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (64, N'Ethiopia', N'et')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (65, N'Finland', N'fi')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (66, N'Fiji', N'fj')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (67, N'Falkland Islands (Malvinas)', N'fk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (68, N'Micronesia', N'fm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (69, N'Faroe Islands', N'fo')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (70, N'France', N'fr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (71, N'Gabon', N'ga')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (72, N'United Kingdom (no new registrations', N'gb')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (73, N'Grenada', N'gd')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (74, N'Georgia', N'ge')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (75, N'French Guiana', N'gf')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (76, N'Guernsey', N'gg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (77, N'Ghana', N'gh')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (78, N'Gibraltar', N'gi')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (79, N'Greenland', N'gl')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (80, N'Gambia', N'gm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (81, N'Guinea', N'gn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (82, N'Guadeloupe', N'gp')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (83, N'Equatorial Guinea', N'gq')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (84, N'Greece', N'gr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (85, N'South Georgia And The South Sandwich Islands', N'gs')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (86, N'Guatemala', N'gt')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (87, N'Guinea-Bissau', N'gw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (88, N'Guyana', N'gy')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (89, N'Hong Kong', N'hk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (90, N'Honduras', N'hn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (91, N'Croatia (local name: Hrvatska)', N'hr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (92, N'Haiti', N'ht')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (93, N'Hungary', N'hu')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (94, N'Indonesia', N'id')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (95, N'Ireland', N'ie')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (96, N'Israel', N'il')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (97, N'Isle of Man', N'im')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (98, N'India', N'in')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (99, N'Iraq', N'iq')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (100, N'Iran (Islamic Republic Of)', N'ir')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (101, N'Iceland', N'is')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (102, N'Italy', N'it')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (103, N'Jersey', N'je')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (104, N'Jamaica', N'jm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (105, N'Jordan', N'jo')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (106, N'Japan', N'jp')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (107, N'Kenya', N'ke')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (108, N'Kyrgyzstan', N'kg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (109, N'Cambodia', N'kh')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (110, N'Kiribati', N'ki')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (111, N'Comoros', N'km')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (112, N'Saint Kitts And Nevis', N'kn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (113, N'North Korea', N'kp')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (114, N'Korea', N'kr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (115, N'Kuwait', N'kw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (116, N'Cayman Islands', N'ky')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (117, N'Kazakhstan', N'kz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (118, N'Lao People''s Democratic Republic', N'la')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (119, N'Lebanon', N'lb')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (120, N'Saint Lucia', N'lc')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (121, N'Liechtenstein', N'li')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (122, N'Sri Lanka', N'lk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (123, N'Liberia', N'lr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (124, N'Lesotho', N'ls')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (125, N'Lithuania', N'lt')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (126, N'Luxembourg', N'lu')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (127, N'Latvia', N'lv')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (128, N'Libyan Arab Jamahiriya', N'ly')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (129, N'Morocco', N'ma')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (130, N'Monaco', N'mc')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (131, N'Moldova', N'md')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (132, N'Montenegro', N'me')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (133, N'Madagascar', N'mg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (134, N'Marshall Islands', N'mh')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (135, N'Macedonia', N'mk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (136, N'Mali', N'ml')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (137, N'Myanmar', N'mm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (138, N'Mongolia', N'mn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (139, N'Macau', N'mo')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (140, N'Northern Mariana Islands', N'mp')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (141, N'Martinique', N'mq')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (142, N'Mauritania', N'mr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (143, N'Montserrat', N'ms')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (144, N'Malta', N'mt')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (145, N'Mauritius', N'mu')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (146, N'Maldives', N'mv')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (147, N'Malawi', N'mw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (148, N'Mexico', N'mx')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (149, N'Malaysia', N'my')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (150, N'Mozambique', N'mz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (151, N'Namibia', N'na')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (152, N'New Caledonia', N'nc')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (153, N'Niger', N'ne')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (154, N'Norfolk Island', N'nf')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (155, N'Nigeria', N'ng')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (156, N'Nicaragua', N'ni')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (157, N'Netherlands', N'nl')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (158, N'Norway', N'no')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (159, N'Nepal', N'np')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (160, N'Nauru', N'nr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (161, N'Niue', N'nu')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (162, N'New Zealand', N'nz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (163, N'Oman', N'om')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (164, N'Panama', N'pa')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (165, N'Peru', N'pe')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (166, N'French Polynesia', N'pf')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (167, N'Papua New Guinea', N'pg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (168, N'Philippines', N'ph')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (169, N'Pakistan', N'pk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (170, N'Poland', N'pl')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (171, N'St. Pierre And Miquelon', N'pm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (172, N'Pitcairn', N'pn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (173, N'Palestine', N'ps')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (174, N'Portugal', N'pt')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (175, N'Palau', N'pw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (176, N'Paraguay', N'py')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (177, N'Qatar', N'qa')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (178, N'Reunion', N're')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (179, N'Romania', N'ro')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (180, N'Serbia', N'rs')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (181, N'Russian Federation', N'ru')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (182, N'Rwanda', N'rw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (183, N'Saudi Arabia', N'sa')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (184, N'Solomon Islands', N'sb')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (185, N'Seychelles', N'sc')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (186, N'Sudan', N'sd')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (187, N'Sweden', N'se')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (188, N'Singapore', N'sg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (189, N'St. Helena', N'sh')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (190, N'Slovenia', N'si')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (191, N'Svalbard And Jan Mayen Islands', N'sj')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (192, N'Slovakia (Slovak Republic)', N'sk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (193, N'Sierra Leone', N'sl')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (194, N'San Marino', N'sm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (195, N'Senegal', N'sn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (196, N'Somalia', N'so')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (197, N'Suriname', N'sr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (198, N'Sao Tome And Principe', N'st')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (199, N'El Salvador', N'sv')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (200, N'Syrian Arab Republic', N'sy')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (201, N'Swaziland', N'sz')
GO
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (202, N'Turks And Caicos Islands', N'tc')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (203, N'Chad', N'td')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (204, N'French Southern Territories', N'tf')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (205, N'Togo', N'tg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (206, N'Thailand', N'th')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (207, N'Tajikistan', N'tj')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (208, N'Tokelau', N'tk')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (209, N'Turkmenistan', N'tm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (210, N'Tunisia', N'tn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (211, N'Tonga', N'to')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (212, N'Turkey', N'tr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (213, N'Trinidad And Tobago', N'tt')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (214, N'Tuvalu', N'tv')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (215, N'Taiwan', N'tw')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (216, N'Tanzania', N'tz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (217, N'Ukraine', N'ua')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (218, N'Uganda', N'ug')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (219, N'United States', N'us')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (220, N'Uruguay', N'uy')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (221, N'Uzbekistan', N'uz')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (222, N'Saint Vincent And The Grenadines', N'vc')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (223, N'Venezuela', N've')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (224, N'Virgin Islands (British)', N'vg')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (225, N'Virgin Islands (U.S.)', N'vi')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (226, N'Viet Nam', N'vn')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (227, N'Vanuatu', N'vu')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (228, N'Wallis And Futuna Islands', N'wf')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (229, N'Samoa', N'ws')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (230, N'Yemen', N'ye')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (231, N'Mayotte', N'yt')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (232, N'South Africa', N'za')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (233, N'Zambia', N'zm')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (234, N'Zaire', N'zr')
INSERT [dbo].[Country] ([CountryId], [CountryName], [CountryAbbreiv]) VALUES (235, N'Zimbabwe', N'zw')
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [AGE], [ADDRESS], [SALARY], [DATE], [Activity]) VALUES (1, N'Ramesh', 32, N'Ahmedabad                ', CAST(2000.00 AS Decimal(18, 2)), CAST(N'2019-06-18 10:57:13.2170000' AS DateTime2), 1)
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [AGE], [ADDRESS], [SALARY], [DATE], [Activity]) VALUES (2, N'Khilan', 25, N'Delhi                    ', CAST(1500.00 AS Decimal(18, 2)), CAST(N'2019-06-18 10:57:13.2170000' AS DateTime2), 1)
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [AGE], [ADDRESS], [SALARY], [DATE], [Activity]) VALUES (3, N'kaushik', 23, N'Kota                     ', CAST(2000.00 AS Decimal(18, 2)), CAST(N'2019-06-18 10:57:13.2170000' AS DateTime2), 1)
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [AGE], [ADDRESS], [SALARY], [DATE], [Activity]) VALUES (4, N'Chaitali', 25, N'Mumbai                   ', CAST(6500.00 AS Decimal(18, 2)), CAST(N'2019-06-18 10:57:13.2170000' AS DateTime2), 1)
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [AGE], [ADDRESS], [SALARY], [DATE], [Activity]) VALUES (5, N'Hardik', 27, N'Bhopal                   ', CAST(8500.00 AS Decimal(18, 2)), CAST(N'2019-06-18 10:57:13.2170000' AS DateTime2), 0)
INSERT [dbo].[CUSTOMERS] ([ID], [NAME], [AGE], [ADDRESS], [SALARY], [DATE], [Activity]) VALUES (6, N'Komal', 22, N'MP                       ', CAST(4500.00 AS Decimal(18, 2)), CAST(N'2019-06-18 10:57:46.0570000' AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[Emails] ON 

INSERT [dbo].[Emails] ([ID], [Email], [UserID]) VALUES (1, N'a.alkaff@sdkjordan.com', 10)
INSERT [dbo].[Emails] ([ID], [Email], [UserID]) VALUES (2, N'AliMoh@tiraforit.com', 11)
INSERT [dbo].[Emails] ([ID], [Email], [UserID]) VALUES (3, N'a.alkaff@tiraforit.com', 10)
INSERT [dbo].[Emails] ([ID], [Email], [UserID]) VALUES (4, N'ahtha20@yahoo.com', 11)
INSERT [dbo].[Emails] ([ID], [Email], [UserID]) VALUES (5, N'a.alkaff@tria.com', 20)
SET IDENTITY_INSERT [dbo].[Emails] OFF
SET IDENTITY_INSERT [dbo].[Phones] ON 

INSERT [dbo].[Phones] ([ID], [Phone], [UserID]) VALUES (1, N'0788686870', 20)
SET IDENTITY_INSERT [dbo].[Phones] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [BirthDate], [Gender], [CountryID], [Login], [Password], [CreateDate], [ModifyDate], [Actvie], [SupervisorID]) VALUES (10, N'Ahmed Alkaff', NULL, NULL, NULL, NULL, N'a.alkaff', N'123abc', NULL, NULL, 1, 10)
INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [BirthDate], [Gender], [CountryID], [Login], [Password], [CreateDate], [ModifyDate], [Actvie], [SupervisorID]) VALUES (11, N'Ali Mohammed', NULL, NULL, NULL, NULL, N'AliMo', N'123abc', NULL, NULL, 1, 10)
INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [BirthDate], [Gender], [CountryID], [Login], [Password], [CreateDate], [ModifyDate], [Actvie], [SupervisorID]) VALUES (12, N'Khaled', NULL, NULL, NULL, NULL, N'khalid', N'123abc', NULL, NULL, 1, 11)
INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [BirthDate], [Gender], [CountryID], [Login], [Password], [CreateDate], [ModifyDate], [Actvie], [SupervisorID]) VALUES (13, N'Fadi', NULL, NULL, NULL, NULL, N'Ali', N'abc123', CAST(N'2019-06-23 00:00:00.0000000' AS DateTime2), CAST(N'2019-06-23 00:00:00.0000000' AS DateTime2), 1, 10)
INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [BirthDate], [Gender], [CountryID], [Login], [Password], [CreateDate], [ModifyDate], [Actvie], [SupervisorID]) VALUES (14, N'Shadi', NULL, NULL, NULL, NULL, N'Shadi', N'12544abc', CAST(N'2019-06-23 00:00:00.0000000' AS DateTime2), CAST(N'2019-06-23 00:00:00.0000000' AS DateTime2), 1, 10)
INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [BirthDate], [Gender], [CountryID], [Login], [Password], [CreateDate], [ModifyDate], [Actvie], [SupervisorID]) VALUES (19, N'Abdullah', NULL, NULL, NULL, NULL, N'abed', N';ldkfdf', CAST(N'2019-06-23 11:41:22.0000000' AS DateTime2), CAST(N'2019-06-23 11:41:22.0000000' AS DateTime2), 1, 10)
INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [BirthDate], [Gender], [CountryID], [Login], [Password], [CreateDate], [ModifyDate], [Actvie], [SupervisorID]) VALUES (20, N'Ahmed', N'Alkaff', CAST(N'2019-05-29' AS Date), 1, 78, N'AhmedAlkaff', N'123abc', CAST(N'2019-07-07 11:59:58.1530000' AS DateTime2), CAST(N'2019-07-07 11:59:58.1530000' AS DateTime2), 1, 10)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[Emails]  WITH CHECK ADD  CONSTRAINT [FK_Emails_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Emails] CHECK CONSTRAINT [FK_Emails_Users]
GO
ALTER TABLE [dbo].[Phones]  WITH CHECK ADD  CONSTRAINT [FK_Phones_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Phones] CHECK CONSTRAINT [FK_Phones_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Country] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Country]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Users] FOREIGN KEY([SupervisorID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Users]
GO
/****** Object:  StoredProcedure [dbo].[AddUser]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddUser] 
@FName varchar(500),
@LName varchar(500),
@Login nvarchar(100),
@BirthDay date,
@Gender bit,
@Email varchar(500),
@Phone varchar(500),
@Password varchar(500),
@CountryID int 


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


	SET NOCOUNT ON;

	declare @EmailID int = (select count(ID) from Emails where Email =@Email);
	if @EmailID > 0 
	return -5;
	
	
	declare @PhoneID int = (select count(ID) from Phones where Phone =@Phone);
	if @PhoneID > 0 
	return -4;
	

	INSERT INTO [dbo].Users VALUES (@FName , @LName , @BirthDay , @Gender , @CountryID ,@Login, @Password ,GETDATE(),GETDATE(),1,10) ;
	DECLARE @ID int =  @@IDENTITY;

	INSERT INTO [dbo].[Emails] VALUES (@Email , @ID ) ;
	INSERT INTO [dbo].[Phones] VALUES (@Phone , @ID ) ;


	return @ID ;

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllUsers]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllUsers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users ;
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserByID]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserByID]
	@id int, 
	@result nvarchar(50) out 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users where ID = @id;
	set @result = (Select Name from Users where ID = @id) ;
END

GO
/****** Object:  StoredProcedure [dbo].[SearchUserName]    Script Date: 7/7/2019 12:06:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SearchUserName]
	@key nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users where Name like '%'+@key+'%' ;
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[49] 4[10] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Emails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 185
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Users"
            Begin Extent = 
               Top = 124
               Left = 323
               Bottom = 328
               Right = 493
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Users_1"
            Begin Extent = 
               Top = 8
               Left = 557
               Bottom = 138
               Right = 727
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2235
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vUsers'
GO
USE [master]
GO
ALTER DATABASE [asp032019] SET  READ_WRITE 
GO
