﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MultiView.aspx.cs" Inherits="MultiView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 170px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
                <asp:View ID="View1" runat="server">
                    <table style="width: 80%">
                        <tbody>
                            <tr>
                                <td class="auto-style1">
                                    <label>Name:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxName" runat="server" Width="80%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>Email:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxEmail" runat="server" Width="80%" TextMode="Email"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>Phone:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxPhone" runat="server" Width="80%" TextMode="Phone"></asp:TextBox></td>
                            </tr>

                        </tbody>
                    </table>
                    <br />
                    <div class="row" style="text-align: right; padding: 10px 10px 10px 10px; margin-right: 20%">
                        <asp:ImageButton ID="ImageButtonNext1" ImageUrl="~/images/next.jpg" Height="30px" runat="server" Style="text-align: right" OnClick="ImageButton_Click" />

                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <table style="width: 80%">
                        <tbody>
                            <tr>
                                <td class="auto-style1">
                                    <label>Age:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxAge" runat="server" Width="80%" TextMode="Number"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>Address:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxAddress" runat="server" Width="80%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>BirthDate:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxBirthDate" runat="server" Width="80%" TextMode="Date"></asp:TextBox></td>
                            </tr>

                        </tbody>
                    </table>
                    <br />
                    <div class="row" style="width: 80%; float: left; margin-right: 20%; display: inline">
                        <asp:ImageButton ID="ImageButtonNext2" ImageUrl="~/images/next.jpg" Height="30px" runat="server" Style="text-align: right; float: right" OnClick="ImageButton_Click" />
                        <asp:ImageButton ID="ImageButtonPrevious1" ImageUrl="~/images/previous.jpg" Height="30px" runat="server" Style="text-align: left; float: left" OnClick="ImageButton_Click" />
                    </div>

                </asp:View>
                <asp:View ID="View3" runat="server">
                   <table style="width: 80%">
                        <tbody>
                            <tr>
                                <td class="auto-style1"><label>Name:</label></td>
                                <td><asp:Label ID="LabelName" runat="server" Text=""></asp:Label></td>
                                <td class="auto-style1"><label>Email:</label></td>
                                <td><asp:Label ID="LabelEmail" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="auto-style1"><label>Phone:</label></td>
                                <td><asp:Label ID="LabelPhone" runat="server" Text=""></asp:Label></td>
                                <td class="auto-style1"><label>Age:</label></td>
                                <td><asp:Label ID="LabelAge" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="auto-style1"><label>Address:</label></td>
                                <td><asp:Label ID="LabelAddress" runat="server" Text=""></asp:Label></td>
                                <td class="auto-style1"><label>Birthdate:</label></td>
                                <td><asp:Label ID="LabelBirthDate" runat="server" Text=""></asp:Label></td>
                         
                            </tr>

                        </tbody>
                    </table>
                    <br />
                    <div class="row" style="width: 80%; float: left; margin-right: 20%; display: inline">
                        <asp:ImageButton ID="ImageButtonFinish" ImageUrl="~/images/finish.png" Height="30px" runat="server" Style="text-align: right; float: right" OnClick="ImageButton_Click"/>
                        <asp:ImageButton ID="ImageButtonPrevious2" ImageUrl="~/images/previous.jpg" Height="30px" runat="server" Style="text-align: left; float: left" OnClick="ImageButton_Click"/>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </form>
</body>
</html>
