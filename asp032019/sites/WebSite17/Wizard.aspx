﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Wizard.aspx.cs" Inherits="Wizard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Wizard ID="Wizard1" runat="server" BackColor="#EFF3FB" BorderColor="#B5C7DE" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" Width="80%" OnActiveStepChanged="Wizard1_ActiveStepChanged" OnFinishButtonClick="Wizard1_FinishButtonClick" ActiveStepIndex="2">
            <FinishNavigationTemplate>
                <asp:Button ID="FinishPreviousButton" runat="server" BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" CommandName="MovePrevious" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" Text="Previous" />
                <asp:Button ID="FinishButton" runat="server" BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" CommandName="MoveComplete" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" Text="Finish" />
            </FinishNavigationTemplate>
            <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
            <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
            <SideBarButtonStyle BackColor="#507CD1" Font-Names="Verdana" ForeColor="White" />
            <SideBarStyle BackColor="#507CD1" Font-Size="0.9em" VerticalAlign="Top" Width="30%" />
            <StartNavigationTemplate>
                <asp:Button ID="StartNextButton" runat="server" BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" CommandName="MoveNext" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" Text="Next" />
            </StartNavigationTemplate>
            <StepNavigationTemplate>
                <asp:Button ID="StepPreviousButton" runat="server" BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" CommandName="MovePrevious" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" Text="Previous" />
                <asp:Button ID="StepNextButton" runat="server" BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px" CommandName="MoveNext" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" Text="Next" />
            </StepNavigationTemplate>
            <StepStyle Font-Size="0.8em" ForeColor="#333333" />
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="Personal Information">
           <table style="width: 80%">
                        <tbody>
                            <tr>
                                <td class="auto-style1">
                                    <label>Name:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxName" runat="server" Width="80%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>Email:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxEmail" runat="server" Width="80%" TextMode="Email"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>Phone:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxPhone" runat="server" Width="80%" TextMode="Phone"></asp:TextBox></td>
                            </tr>

                        </tbody>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="Contact Infroamtion">
                                        <table style="width: 80%">
                        <tbody>
                            <tr>
                                <td class="auto-style1">
                                    <label>Age:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxAge" runat="server" Width="80%" TextMode="Number"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>Address:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxAddress" runat="server" Width="80%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <label>BirthDate:</label></td>
                                <td>
                                    <asp:TextBox ID="TextBoxBirthDate" runat="server" Width="80%" TextMode="Date"></asp:TextBox></td>
                            </tr>

                        </tbody>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep3" runat="server" Title="Review">
                        <table style="width: 80%">
                        <tbody>
                            <tr>
                                <td class="auto-style1"><label>Name:</label></td>
                                <td><asp:Label ID="LabelName" runat="server" Text=""></asp:Label></td>
                                <td class="auto-style1"><label>Email:</label></td>
                                <td><asp:Label ID="LabelEmail" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="auto-style1"><label>Phone:</label></td>
                                <td><asp:Label ID="LabelPhone" runat="server" Text=""></asp:Label></td>
                                <td class="auto-style1"><label>Age:</label></td>
                                <td><asp:Label ID="LabelAge" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="auto-style1"><label>Address:</label></td>
                                <td><asp:Label ID="LabelAddress" runat="server" Text=""></asp:Label></td>
                                <td class="auto-style1"><label>Birthdate:</label></td>
                                <td><asp:Label ID="LabelBirthdate" runat="server" Text=""></asp:Label></td>
                         
                            </tr>

                        </tbody>
                    </table>
                </asp:WizardStep>
            </WizardSteps>
        </asp:Wizard>
    </div>
    </form>
</body>
</html>
