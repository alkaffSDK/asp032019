﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MultiView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton) sender;
        if(btn.ClientID.Contains("ImageButtonNext"))
            MultiView1.ActiveViewIndex++; 
        else if (btn.ClientID.Contains("ImageButtonPrevious"))
            MultiView1.ActiveViewIndex--;
        else
        {

        }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {

        if(MultiView1.ActiveViewIndex == MultiView1.Views.Count - 1)
        {
            LabelName.Text = TextBoxName.Text;
            LabelPhone.Text = TextBoxPhone.Text;
            LabelEmail.Text = TextBoxEmail.Text;
            LabelAge.Text = TextBoxAge.Text;
            LabelAddress.Text = TextBoxAddress.Text;
            LabelBirthDate.Text = TextBoxBirthDate.Text;
        }
    }
}