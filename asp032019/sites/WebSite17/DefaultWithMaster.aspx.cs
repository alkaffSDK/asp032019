﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DefaultWithMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        // First way
        //if(null != Master )
        //{
        //    TextBox tb = Master.FindControl("TextBoxName") as TextBox;
        //    TextBox1.Text = tb.Text;
        //}

        // Second way

        //if(Master is  MyMasterPage)
        //{
        //    MyMasterPage mp = (MyMasterPage)Master;
        //    TextBox1.Text = mp.MyProperty;
        //}

        // Third way by add <%@ MasterType VirtualPath="~/MyMasterPage.master" %> to the start of the page

       // TextBox1.Text = Master.MyProperty;

    }


    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
      
        TextBox1.Text = GridView1.SelectedRow.Cells[2].Text;
    }

    protected void LinkButtonInsert_Click(object sender, EventArgs e)
    {
        SqlDataSource1.InsertParameters["Name"].DefaultValue = (GridView1.FooterRow.FindControl("TextBoxName") as TextBox).Text;
        SqlDataSource1.InsertParameters["Age"].DefaultValue = (GridView1.FooterRow.FindControl("TextBoxAge") as TextBox).Text;
        SqlDataSource1.InsertParameters["Address"].DefaultValue = (GridView1.FooterRow.FindControl("TextBoxAddress") as TextBox).Text;
        SqlDataSource1.InsertParameters["Salary"].DefaultValue = (GridView1.FooterRow.FindControl("TextBoxSalary") as TextBox).Text;
        SqlDataSource1.InsertParameters["Activity"].DefaultValue = (GridView1.FooterRow.FindControl("CheckBoxActivity") as CheckBox).Checked.ToString();

        SqlDataSource1.Insert();
        
    }
}