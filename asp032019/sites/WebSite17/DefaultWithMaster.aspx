﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterPage.master" AutoEventWireup="true" CodeFile="DefaultWithMaster.aspx.cs" Inherits="DefaultWithMaster" %>
<%@ MasterType VirtualPath="~/MyMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    Default Page with master
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent1" Runat="Server">

    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
    </div>
    <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" ShowFooter="True" AllowPaging="True" AllowSorting="True">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Select" Text="Select"></asp:LinkButton>
                    &nbsp;<asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                </ItemTemplate>
                <FooterTemplate>
                    <div style="text-align:center;width:90%">
                        <asp:LinkButton ID="LinkButtonInsert" runat="server" OnClick="LinkButtonInsert_Click">Insert</asp:LinkButton>
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
            <asp:TemplateField HeaderText="NAME" SortExpression="NAME">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("NAME") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <div style="text-align:center;width:90%">
                        <asp:TextBox ID="TextBoxName" runat="server" placeholder="Name"></asp:TextBox>
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AGE" SortExpression="AGE">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("AGE") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("AGE") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <div style="text-align:center;width:90%">
                        <asp:TextBox ID="TextBoxAge" runat="server" placeholder="Age"></asp:TextBox>
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ADDRESS" SortExpression="ADDRESS">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <div style="text-align:center;width:90%">
                        <asp:TextBox ID="TextBoxAddress" runat="server" placeholder="Address"></asp:TextBox>
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="SALARY" SortExpression="SALARY">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SALARY") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("SALARY") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <div style="text-align:center;width:90%">
                        <asp:TextBox ID="TextBoxSalary" runat="server" placeholder="Salary"></asp:TextBox>
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField HeaderText="Activity" SortExpression="Activity">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Activity") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Activity") %>' Enabled="false" />
                </ItemTemplate>
                <FooterTemplate>
                    <div style="text-align:center;width:90%">
                      <asp:CheckBox ID="CheckBoxActivity" runat="server" Checked="true" Text="Activity" />
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
             <asp:BoundField DataField="DATE" HeaderText="DATE" SortExpression="DATE" />
        </Columns>
        <EditRowStyle BackColor="LightYellow" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" Height="30px" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:asp032019ConnectionString %>" 
        SelectCommand="SELECT * FROM [CUSTOMERS] " 
        DeleteCommand="DELETE FROM [CUSTOMERS] WHERE [ID] = @ID" 
        InsertCommand="INSERT INTO [CUSTOMERS] ([NAME], [AGE], [ADDRESS], [SALARY], [DATE], [Activity]) VALUES (@Name, @Age, @Address, @Salary, GETDATE(), @Activity)" 
        UpdateCommand="UPDATE [CUSTOMERS] SET [NAME] = @NAME, [AGE] = @AGE, [ADDRESS] = @ADDRESS, [SALARY] = @SALARY, [DATE] = @DATE, [Activity] = @Activity WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ID" Type="Int32" />
            <asp:Parameter  Name="Name"  DbType="String" DefaultValue="NULL"  />
            <asp:Parameter  Name="Age"   DbType="Int32" DefaultValue="0" />
            <asp:Parameter  Name="Address"  DbType="String"  ConvertEmptyStringToNull="true" />
            <asp:Parameter  Name="Salary"  DbType="Currency"   />
            <asp:Parameter  Name="Activity"  DbType="Boolean" DefaultValue="true"   />
        </InsertParameters>
        <SelectParameters>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="NAME" Type="String" />
            <asp:Parameter Name="AGE" Type="Int32" />
            <asp:Parameter Name="ADDRESS" Type="String" />
            <asp:Parameter Name="SALARY" Type="Decimal" />
            <asp:Parameter DbType="DateTime2" Name="DATE" />
            <asp:Parameter Name="Activity" Type="Boolean" />
            <asp:Parameter Name="ID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContentTitle" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Header"></asp:Label>
</asp:Content>

