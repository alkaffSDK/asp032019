﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace WCFHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(WCFService.MyService)))
            {
                host.Open();
                Console.WriteLine("The service host state is {0} @ {1}",host.State , DateTime.Now);

                do
                {
                    Console.WriteLine("Press Esc to stop the service host.");
                } while (! Console.ReadKey().Key.Equals(ConsoleKey.Escape));

                Console.WriteLine("The service host is :"+ host.State);
                Console.WriteLine("Thank you");
                Console.ReadKey();
            }

        }
    }
}
