﻿1) create a reporistory with your name @ bitbukcit.org
2) add "AlkaffSDK" with write permission to your project.
	from Settings ->User and group access -> Users

3) Add a class named TASK1 to solve this questions:
	a) Write a C# Sharp program to print Hello and your name in a separate line. 
		Expected Output : 
		Hello:
		Ahmed Alkaff


	b)Write a C# Sharp program to print the result of the specified operations.
		Test data:

		-1 + 4 * 6
		( 35+ 5 ) % 7
		14 + -4 * 6 / 11
		2 + 15 / 6 * 1 - 7 % 2
	Expected Output:
		23
		5
		12
		3	


	c) Write a C# Sharp program to print the output of multiplication of three numbers which will be entered by the user. 
	Test Data:
	Input the first number to multiply: 2
	Input the second number to multiply: 3
	Input the third number to multiply: 6
	Expected Output:
	2 x 3 x 6 = 36