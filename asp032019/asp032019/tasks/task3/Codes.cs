﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationTest
{
    class Program
    {

        public class Alien
        {
            public static  int SNAKE_ALIEN = 0;            			// افعى
            public static  int OGRE_ALIEN = 1;             			// غول
            public static  int MARSHMALLOW_MAN_ALIEN = 2;  			// رجل المارشملو
            public int type; 										// Stores one of the three above types
            public int health; 										// 0=dead, 100=full strength
            public String name;

            public Alien(int type, int health, String name)
            {
                this.type = type;
                this.health = health;
                this.name = name;
            }
        }

        public class AlienPack
        {
            private Alien[] aliens;
            public AlienPack(int numAliens)
            {
                aliens = new Alien[numAliens];
            }
            public void addAlien(Alien newAlien, int index)
            {
                aliens[index] = newAlien;
            }
            public Alien[] getAliens()
            {
                return aliens;
            }
            public int calculateDamage()
            {
                int damage = 0;
                for (int i = 0; i < aliens.Length; i++)
                {
                    if (aliens[i].type == Alien.SNAKE_ALIEN)
                    {
                        damage += 10;								// Snake does 10 damage
                    }
                    else if (aliens[i].type == Alien.OGRE_ALIEN)
                    {
                        damage += 6;								// Ogre (غول )does 6 damage
                    }
                    else if (aliens[i].type == Alien.MARSHMALLOW_MAN_ALIEN)
                    {
                        damage += 1;              					// Marshmallow Man does 1 damage
                    }
                }
                return damage;
            }
        }

        static void Main(string[] args)
        {
        }
    }
}
