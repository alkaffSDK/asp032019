﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.tasks.task4
{
    class Solution
    {
        static void Main(string[] args)
        {

            // TODO: put your file data source path
            String filepath = @"C:\Users\aalka\Source\Repos\asp032019\asp032019\asp032019\tasks\task4\DataSet.txt";

            // TODO : Create collection of Records

            List<Record> records = new List<Record>();
            string line = "";
            using (StreamReader sr = new StreamReader(filepath))
            {

                Record r;
                while ((line = sr.ReadLine()) != null)
                {
                    // TODO: read each line and stored it a Record object in the records collection
                    Console.WriteLine(line);

                    // r = new Record();
                    // records.Add(r);

                }


                int males = count(records, new Male());
                int females = count(records, new Female());

                // TODO : Build the condition to text with as Object of Conititon iterface
                // TODO : call the count method
                // TODO : call the print method
                // TODO : call the report method
            }

            Console.ReadKey();
        }

        public static int count(List<Record> list, Condition<Record> condition)
        {
            int counter = 0;
            foreach (var item in list)
            {
                if (condition.test(item))
                    counter++;
            }

            return counter;
        }


        public static void print(List<Record> list, Condition<Record> condition)
        {
            foreach (var item in list)
            {
                if (condition.test(item))
                    Console.WriteLine(item.ToString());
            }

        }

        public static void print(List<Record> list, Condition<Record> condition, String outputFile)
        {
            using (StreamWriter sw = new StreamWriter(outputFile))
            {
                foreach (var item in list)
                {
                    if (condition.test(item))
                        sw.WriteLine(item.ToString());
                }
            }
        }

        public static void report(List<Record> list, Condition<Record> condition)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

    }
}
