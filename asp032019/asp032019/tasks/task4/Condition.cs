
using System;
using asp032019.tasks.task4;

namespace asp032019.tasks.task4
{
    public interface Condition<T>
    {
        bool test(T t);
    }

    public class Male : Condition<Record>
    {
        public bool test(Record r)
        {
            return r.getSex().Equals("Male");
        }
    }

    public class Female : Condition<Record>
    {
        public bool test(Record r)
        {
            return r.getSex().Equals("Female");
        }
    }

}
