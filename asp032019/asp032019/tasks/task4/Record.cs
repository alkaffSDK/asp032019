using System;

namespace asp032019.tasks.task4
{
    public class Record
    {
        private int ID, Year;
        private bool? Sex;
        byte Education, Score;

        public Record(string ID, string year, string sex, string education, string score)
        {
            setID(ID);
            setYear(year);
            setSex(sex);
            setEducation(education);
            setScore(score);
        }


        public override String ToString()
        {
            return "Record{" +
                    "ID=" + ID +
                    ", Year=" + Year +
                    ", Sex=" + getSex() +
                    ", Education=" + Education +
                    ", Score=" + Score +
                    '}';
        }

        public int getID()
        {
            return ID;
        }

        public void setID(int ID)
        {
            this.ID = ID;
        }

        public void setID(String ID)
        {
            setID(ID.ToString().Trim().Replace("\"", ""));
        }

        public int getYear()
        {
            return Year;
        }

        public void setYear(int year)
        {
            Year = year;
        }

        public void setYear(String year)
        {
            setYear(year.ToString().Trim().Replace("\"", ""));
        }

        public String getSex()
        {
            return Sex == null ? "Not specified" : Sex == true ? "Female" : "Male";
        }

        public bool? getGenderBoolean()
        {
            return Sex;
        }


        public void setSex(Boolean sex)
        {
            Sex = sex;
        }

        public void setSex(String sex)
        {
            if (sex.Equals("female", StringComparison.CurrentCultureIgnoreCase))
                Sex = true;
            else if (sex.Equals("male", StringComparison.CurrentCultureIgnoreCase))
                Sex = false;
            else
                Sex = null;
        }

        public byte getEducation()
        {
            return Education;
        }

        public void setEducation(byte education)
        {
            Education = education;
        }

        public void setEducation(String education)
        {
            setEducation(education.ToString().Trim().Replace("\"", ""));
        }

        public byte getScore()
        {
            return Score;
        }

        public void setScore(byte score)
        {
            Score = score;
        }

        public void setScore(String score)
        {
            setScore(score.ToString().Trim().Replace("\"", ""));
        }
    }
}
