use [asp032019]
go 
CREATE TABLE CUSTOMERS( 
   ID   INT              NOT NULL, 
   NAME VARCHAR (20)     NOT NULL, 
   AGE  INT              NOT NULL, 
   ADDRESS  CHAR (25) , 
   SALARY   DECIMAL (18, 2),        
   PRIMARY KEY (ID));


   exec sp_columns CUSTOMERS

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (1, 'Ramesh', 32, 'Ahmedabad', 2000.00 );
  
INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (2, 'Khilan', 25, 'Delhi', 1500.00 );  

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (3, 'kaushik', 23, 'Kota', 2000.00 );  

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (4, 'Chaitali', 25, 'Mumbai', 6500.00 ); 
 
INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (5, 'Hardik', 27, 'Bhopal', 8500.00 );  

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (6, 'Komal', 22, 'MP', 4500.00 );


select [Name] as [Customer Name], [Age] as [Customer Age] , salary as [Customer Salary] from CUSTOMERS  order by  Age DESC, Name ASC;

update CUSTOMERS 
set [DATE] = GETDATE()
where id = 6 ;

Select LTrim(RTrim(DoctorNameAlternate)) as [Name] from Doctors order by  Name ;

SELECT Name, Age, sum(Salary) from CUSTOMERS 
group by Age , Name;

select DISTINCT  Age from CUSTOMERS ;


select TOP 3 Salary from CUSTOMERS order by Salary DESC;


select TOP 1 Name ,  Salary from CUSTOMERS order by Salary DESC;


select  Name ,  Salary from CUSTOMERS 
where Salary < (SELECT AVG(SALARY) FROM CUSTOMERS)
order by Salary DESC 



DECLARE @AVERAGESALAR int ;
set @AVERAGESALAR = (SELECT AVG(SALARY) FROM CUSTOMERS);
select @AVERAGESALAR;
SELECT [NAME] FROM CUSTOMERS WHERE SALARY >= @AVERAGESALAR ;

SELECT * FROM USERS ;

SELECT * FROM emails ;

select * from users, emails  ;

select * from users, emails  where [Users].[ID] = [emails].[UserID] ;

select U.Name , U.Login,E.Email from users U inner join emails E on U.[ID] = E.[UserID]  
order by U.Name , E.Email;



select U.Name , U.Login,E.Email from users U Left outer join emails E on U.[ID] = E.[UserID]  
order by U.Name , E.Email;

select U.Name , U.Login,E.Email from emails E right outer join  users U on U.[ID] = E.[UserID]  
order by U.Name , E.Email;