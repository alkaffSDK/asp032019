﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace asp032019.sql
{
    class DBConnection
    {

        static string connectionString = "Server=.;Database=asp032019;User Id=sa;Password = 123; ";    // sql server authentication
        static string connectionString1 = "Server=.;Database=asp032019;Trusted_Connection=True; ";     // windows authentication
        static string connectionString2 = " Data Source =.; Initial Catalog = asp032019; Integrated Security = SSPI; Persist Security Info = False;";


        static void Main(string[] args)
        {

            CallSPwithParameters();
            //DbConnection2();

            string login, pass;
            do
            {
                Console.Write("Login:");
                login = Console.ReadLine().Trim();
                Console.Write("Password:");
                pass = Console.ReadLine();
                CheckLogin(login, pass);
                Console.WriteLine("Press any key to continue or Esc to exit.");
            } while (! Console.ReadKey().Key.Equals(ConsoleKey.Escape));

            Console.WriteLine("Thank you.");
            

            Console.ReadKey();
        }

        public static void DbConnection1()
        {
            //  1) connection string  (server name or IP, instance name, login , database name)

  
            // 2) connection object 

            SqlConnection connection = new SqlConnection(connectionString);

            // 3) sql command 

            string sql = "select count(*) from users;";

            // 4) sql command object

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            // or  SqlCommand cmd = new SqlCommand(sql, connection);

            // 5) open the connection 

            connection.Open();

            // 6) execute the command 
            // 7) get the result if any 

            int counter;
            int.TryParse(cmd.ExecuteScalar().ToString(), out counter);
            Console.WriteLine("The user counter is :" + counter);


            // 8) Close 

            connection.Dispose();
            cmd.Dispose();
        }


        public static void DbConnection2()
        {

           

            string counterSQL = "select count(*) from users;";

            string insertSQL = @"INSERT INTO [USERS] values ('{0}','{1}','{2}','{3}','{4}',1,10);";
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(counterSQL, connection))
            {
                connection.Open();


                int counter;
                int.TryParse(cmd.ExecuteScalar().ToString(), out counter);
                Console.WriteLine("The user counter is :" + counter);

                //insertSQL = insertUserData(insertSQL);
                //cmd.CommandText = insertSQL;

                //int numberOfRows = cmd.ExecuteNonQuery();
                //Console.WriteLine("{0} row(s) was effected.", numberOfRows);


                cmd.CommandText = "GetAllUsers";

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Console.WriteLine("{0,4}|{1,15}|{2,10}|{3,10}|{4,22}|{5,22}|{6,2}|{7,4}",
                            reader.GetValue(0), reader.GetValue(1), reader.GetValue(2), 
                            reader.GetValue(3), reader.GetValue(4), reader.GetValue(5),
                             reader.GetValue(6),reader.GetValue(7));
                        //for (int i = 0; i < reader.FieldCount; i++)
                        //{
                        //    Console.Write("{0,10} |", reader.GetValue(i));
                        //}
                        //Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("No data found");
                }
            }
        }

        private static string insertUserData(string insertSQL)
        {
            string name, login, password;
            Console.Write("Your name:");
            name = Console.ReadLine().Trim();
            Console.Write("Your login:");
            login = Console.ReadLine().Trim();
            Console.Write("Your password:");
            password = Console.ReadLine().Trim();

            return string.Format(insertSQL, name, login, password, DateTime.Now, DateTime.Now);

        }
        private static void CheckLogin(string login, string password)
        {
            string sql = string.Format(@"SELECT * FROM [USERS] WHERE [LOGIN] = @login AND [PASSWORD] = @password COLLATE Latin1_General_CS_AS ;",
                login, password);

            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("@login", login);
                cmd.Parameters.AddWithValue("@password", password);
                con.Open();
                SqlDataReader re = cmd.ExecuteReader();
                if (re.HasRows)
                {
                    while (re.Read())
                    {
                        for (int i = 0; i < re.FieldCount; i++)
                            Console.Write("{0,15}| ", re.GetValue(i));
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("Invalid user login.");
                }
            }
        }

        private static void CallSPwithParameters()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmdSP = new SqlCommand("GetUserByID", con))
            {
                con.Open();
                cmdSP.CommandType = CommandType.StoredProcedure;
                cmdSP.Parameters.AddWithValue("@id", 10);

                SqlParameter result = new SqlParameter("@result",SqlDbType.NVarChar,50);
                result.Direction = ParameterDirection.Output;
                cmdSP.Parameters.Add(result);
                cmdSP.ExecuteNonQuery();

                String resultValue = result.Value.ToString();

                Console.WriteLine("The value is :" + resultValue);
            }
        }

        // Bad practices
        private static void CheckLoginBad(string login, string password)
        {
            string sql =string.Format(@"SELECT * FROM [USERS] WHERE [LOGIN] = '{0}' AND [PASSWORD] = REPLACE('{1}',' ','_') COLLATE Latin1_General_CS_AS ;", login,password);
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
               
                con.Open();
                SqlDataReader re = cmd.ExecuteReader();
                if (re.HasRows)
                {
                    while (re.Read())
                    {
                        for (int i = 0; i < re.FieldCount; i++)
                            Console.Write("{0,15}| ", re.GetValue(i));
                        Console.WriteLine();
                    }

                }
                else
                {
                    Console.WriteLine("Invalid user login.");
                }
            }
        }
    }
}
