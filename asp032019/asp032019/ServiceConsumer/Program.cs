﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.ServiceConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (TestService.MyWebServiceSoapClient client = new TestService.MyWebServiceSoapClient())
            {
                Console.WriteLine(client.HelloWorld());
                Console.WriteLine(client.HelloWorld1("Ahmed"));
               
            }

            Console.ReadKey();
        }    
       
    }
}
