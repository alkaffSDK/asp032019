﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_10_2018
{
    class Holder
    {
        public int a;
    }
    class MethodsDemo
    {
        // Method (function): is a named block of statemnts with output and optional inputs  
        // All methods must be defined inside a class 
        // use for code reduction , resuablitity and make code editing easyer 
        // Syntax : 1) defning a method :   
        //              <RETURN_DATA_TYPE> <method_name> ([parameter(variables) list]) {[body]}
        // Syntax : 2) call a method :   
        //              <method_name>([arguments(values) list]);

        // Return data type: could be either void (no return data type) or any data type
        // return statement will end the method execution

        static void PrintHi()
        {
            Console.WriteLine("Hi");
            return;     // you can add return to any method
        }
        static void PrintHi(string name)
        {
            Console.WriteLine("Hi "+name);
        }

        static void PrintHi(string name, int times)
        {
            for(int i=0;i<times;i++)
                Console.WriteLine("Hi " + name);
        }

        static void MultiplyMeBy10(int z)       // z = r (z is a copy of R)
        {
            z *= 10;
            Console.WriteLine("The new Z is: " + z);        // 50
        }
        static void MultiplyMeBy10(ref int z)       // z = r Z is another name for R
        {
            z *= 10;
            Console.WriteLine("The new Z is: "+z);        // 50
        }
        static void MultiplyMeBy10(Holder h)       // 5
        {
            h.a *= 10;
            Console.WriteLine("The Holder.a is: " + h.a);        // 50
        }
        static void Main(string[] args)
        {
            PrintHi();          // hi
            PrintHi("Ahmed",3); // 
            Console.Write("What is your name:");
            string name = Console.ReadLine();
            PrintHi(name);

            Console.Write("What is your name:");
            PrintHi(Console.ReadLine());

            int r=5;
            //Console.Write("R:");
            //int.TryParse(Console.ReadLine(), out r);        // 5 
            Console.WriteLine("pass (value type variable)* by value");
            MultiplyMeBy10( r);
            Console.WriteLine("The value if R is :" + r);     // 5
            Console.WriteLine("pass (value type variable)* by refrence");
            MultiplyMeBy10(ref r);
            Console.WriteLine("The value if R is :"+r);     // 5

            Holder hl = new Holder();
            hl.a = 5;
            MultiplyMeBy10(hl);                                 // 50
            Console.WriteLine("The value if R is :" + hl.a);    // 50


            Console.ReadKey();
        }
    }
}
