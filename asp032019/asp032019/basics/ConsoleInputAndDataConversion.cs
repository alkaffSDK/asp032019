﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.basics
{
    class ConsoleInputAndDataConversion
    {
        static void Main(string[] args)
        {
            // Data conversion 
            double d = 10.5;
            int a = (int)d;
            a = Convert.ToInt32(10.5);

            string value = "0";

            int b = Convert.ToInt32( Convert.ToDouble(value));
            Console.WriteLine("B :{0}",b);
            b = (int)double.Parse(value);
            Console.WriteLine("B :{0}", b);
            Console.WriteLine(int.TryParse(value, out b));
            Console.WriteLine("B :{0}", b);


            byte by = 15;
            short sh = 258;
            int i = 5655;
            long l = 5666566;

            by =  (byte)sh;
            Console.WriteLine("Byte  :{0}", by);
            Console.WriteLine("Short :{0}", sh);
            Console.WriteLine("Int   :{0}", i);
            Console.WriteLine("Long  :{0}", l);


            float f = 152.655F;
            double dd = 154223365.45;
            decimal dec = 155.655566333M;

            //f = dd;
            //dd = dec;

            dd = f;

            dec = Convert.ToDecimal(dd);


            // Here are some cases where the casting is possible
            // but the casted value may not be the same as the original value 
            // based on the real value of the original variable  (bigLong and  BigInt)

            long bigLong = long.MaxValue;
            int BigInt = int.MaxValue;

            Console.WriteLine("bigLong  :{0}", bigLong);
            Console.WriteLine("BigInt   :{0}", BigInt);

            f = BigInt;
            Console.WriteLine("\nBig integer to float");
            Console.WriteLine("F :"+f);
            Console.WriteLine((int)f);

            f = bigLong;
            Console.WriteLine("\nBig long to float");
            Console.WriteLine("F :" + f);
            Console.WriteLine((long)f);

            dd = bigLong;
            Console.WriteLine("\nBig long to double");
            Console.WriteLine("F :" + f);
            Console.WriteLine((long)f);

            /// Console input
            /// 
            int age;
            Console.Write("How old are you? :");
            int.TryParse(Console.ReadLine(), out age);
            Console.WriteLine("You have {0} years.",age);

            ConsoleKey k;
            do
            {
                k = Console.ReadKey().Key;
                Console.WriteLine(k.ToString());
            }
            while ( ! k.Equals(ConsoleKey.Escape));

            char ch = '\u0008';
            ch = (char) 8;



            Console.ReadKey();
        }
    }
}
