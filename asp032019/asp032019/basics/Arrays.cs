﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    
    class Arrays
    {
        static void Main(string[] args)
        {
            // Arrays : set consecutive of memeory locations 

            //int a1, a2 , a3, a4, ...... ,a10;
            // int [] arr = new int [10];
            // Console.WriteLine(a1);

            int[] arr = {1,2,3,4,5,6,7,8,9,10};

            int[] arr1 = new int[70];          // create a variable of type array arr1
                                                // points to an array of 100 integer of zeros


            
             Console.WriteLine(arr);

             Console.WriteLine("{"+arr[0]+ ", "+ arr[1] + ", "+ arr[2] + arr[3] + ", " + arr[4] + ", " + arr[5]+"}");

            //PrintArray(arr);
            //PrintArray(arr1);
            Console.Write("{");
            for (int index = 0; index < arr.Length; index++)
            {
                // Console.Write(arr[index]+(index != arr.Length-1 ?", ":""));
                Console.Write(arr[index] + ", ");
            }

            Console.Write("\b\b} \n");

            int MAX = int.MinValue , MIN = int.MaxValue, SUM = 0, FAILEDCOUNTER = 0;
            // what is your name ?
            Random rand = new Random();
           // Console.Write("{");
            for (int index = 0; index < arr1.Length; index++)
            {
                arr1[index] = rand.Next(35, 101);
                SUM += arr1[index];

                if (arr1[index] > MAX)
                    MAX = arr1[index];

                if (arr1[index] < MIN)
                    MIN = arr1[index];

                FAILEDCOUNTER += arr1[index] < 50 ? 1 : 0;


                // Console.Write(arr1[index] + ", ");
            }

            PrintArray(arr1);
           
           // Console.Write("\b\b} \n");

            Console.WriteLine("The min value:{0}, average: {1} and  max value: {2} ",MIN,(double)SUM/arr1.Length,MAX);
            Console.WriteLine("Failed :{0}/{1} ({3}%) , Passed:{2}/{1} ({4}%)", FAILEDCOUNTER,arr1.Length,arr1.Length-FAILEDCOUNTER, 100 * (double)FAILEDCOUNTER/ arr1.Length, 100 * (double)(arr1.Length -FAILEDCOUNTER) / arr1.Length);
            //MIN = FindMin(arr1);
            //MAX = FindMax(arr1);
            //Console.WriteLine("The min value:{0}, average: {1} and  max value: {2} ", MIN, (double)SUM / arr1.Length, MAX);
            //double[] result = FindMinMax(arr1);
            //Console.WriteLine("The min value:{0}, average: {1} and  max value: {2} ", result[0], result[2], result[1]);

            //MinMax rs = FindMinMax1(arr1);
            //Console.WriteLine("The min value:{0}, average: {1} and  max value: {2} ", rs.min, rs.avg, rs.max);
            

            //int min, max;
            //double average;
            //FindMinMax2(arr1, out min, out max, out average);
            //Console.WriteLine("The min value:{0}, average: {1} and  max value: {2} ", min, average, max);

            Console.ReadKey();

        }


        public static void PrintArray(int[] arr)
        {
            Console.Write("{");
            for (int index = 0; index < arr.Length; index++)
                Console.Write(arr[index] + ", ");
            Console.Write("\b\b} \n");
        }

        public static int FindMin(int[] arr)
        {
            int min = int.MaxValue;
            for (int index = 0; index < arr.Length; index++)
                if (arr[index] < min)
                    min = arr[index];

            return min;
        }

        public static int FindMax(int[] arr)
        {
            int max = int.MinValue;
            for (int index = 0; index < arr.Length; index++)
                if (arr[index] > max)
                    max = arr[index];
            return max;
        }

        public static double[] FindMinMax(int[] arr)
        {
            double[] minmax = { int.MaxValue, int.MinValue, 0.0 };            
            for (int index = 0; index < arr.Length; index++)
            {
                minmax[2] += arr[index];
                if (arr[index] > minmax[1])
                    minmax[1] = arr[index];

                if (arr[index] < minmax[0])
                    minmax[0] = arr[index];
            }

            minmax[2] /= arr.Length;
            return minmax;
        }
        
        public static void FindMinMax2(int[] arr,out int min ,out int max, out double avg)
        {
            min = int.MaxValue;
            max = int.MinValue;
            avg = 0.0;
            

            for (int index = 0; index < arr.Length; index++)
            {
                avg += arr[index];

                if (arr[index] > max)
                    max = arr[index];

                if (arr[index] < min)
                    min = arr[index];
            }

            avg /= arr.Length;

        }
        public static MinMax FindMinMax1(int[] arr)
        {
            MinMax mm = new MinMax();
            mm.avg = 0;
            

            for (int index = 0; index < arr.Length; index++)
            {
                mm.avg += arr[index];
                if (arr[index] > mm.max)
                    mm.max = arr[index];

                if (arr[index] < mm.min)
                    mm.min = arr[index];
            }

            mm.avg /= arr.Length;


            return mm;
        }
    }
    class MinMax
    {
        public int min = int.MaxValue;
        public int max = int.MinValue;
        public double avg;
    }
}
