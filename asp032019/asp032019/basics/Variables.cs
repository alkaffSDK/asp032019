﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019
{

    /// <summary>
    /// Variable is a symbolic name (an identifier) paired with a storage location
    /// (identified by a memory address) which contains some known or unknown quantity of 
    /// information referred to as a value.

    /// Examples :
    /// int x = 10 ;
    /// Object y = new Object();

    /// Syntax :
    /// [modifiers] [access_specifier] <data_type> <variable_identifier> [=<expression>][, <variable_identifier> [=<expression>]]* ;
    /// Reading links :
    /// 
    /// https://www.tutorialspoint.com/csharp/csharp_data_types.htm
    /// </summary>
    class Variables
    {
        static void Main(string[] args)
        {
            bool b = true;

            char ch = 'A';      // A
            Console.WriteLine(ch);

            ch = '\u0041';      // A
            Console.WriteLine(ch);

            float f = 1.1234567890123456789F;
            double d = 1.1234567890123456789;
            decimal m = 1.1234567890123456789M;

            Console.WriteLine("F:"+f);
            Console.WriteLine("D:" + d);
            Console.WriteLine("M:" + m);

            Console.ReadKey();
        }
    }
}
