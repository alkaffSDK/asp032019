﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop
{

    class Parent
    {
        public int Public;
        protected int Protected;
        private int Private;


        public void method()
        {
            Private = 10;
            Protected = 10;
            Public = 10;
        }
    }
    class Child : Parent
    {
        public int InChild;
        public void method()
        {
            // Private = 10;
            Protected = 10;
            Public = 10;
        }
    }
    class InheritanceDemo
    {
        static void Main(string[] args)
        {
            Child c = new Child();
            //c.Private = 10;
            //c.Protected = 10;
            c.InChild = 10;

            Parent p = new Parent();
            // p.InChild = 1;
        }
    }
}
