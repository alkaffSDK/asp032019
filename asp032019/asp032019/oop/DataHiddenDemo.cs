﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop
{
    class Point
    {
        public int x, y, z;
        public int ID;

        public override bool Equals(object obj)
        {
            if (obj is Point)
            {
                Point p = (Point)obj;
                return x == p.x && y == p.y && z == p.z;
            }
               // return x == ((Point)obj).x && y == ((Point)obj).y && z == ((Point)obj).z;
            else
                return false;
        }

        public override string ToString()
        {
            return string.Format("Point ({0},{1},{2})", x, y, z);
        }
    }

   
    class DataHiddenDemo
    {
        static void Main(string[] args)
        {
            //int x, y, z;

            Point p = new Point();
            p.x = 1;
            p.y = 2;
            p.z = 3;


            //Console.WriteLine("Point [{0},{1},{2}]",p.x,p.y,p.z);
            Console.WriteLine(p);
            // int x1, y1, z1;
            Point p1 = new Point();
            p1.x = 1;
            p1.y = 2;
            p1.z = 3;


           
            
            //Console.WriteLine("Point [{0},{1},{2}]", p1.x, p1.y, p1.z);
            //Console.WriteLine(p.ToString());
            Console.WriteLine(p1);

            Console.WriteLine(p == p1);
            Console.WriteLine(p.Equals(p1));
            Console.WriteLine(p.Equals("1,2,3"));

            User user = new User();
          
            Console.ReadKey();
        }
    }
}
