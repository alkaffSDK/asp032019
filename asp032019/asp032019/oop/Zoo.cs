﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop
{

    interface IFlyable1
    {
         string CanFly();
    }
    class Animal1
    {
        public override string ToString()
        {
            return "Animal";
        }

        public virtual string Print()
        {
            return "Animal";
        }
    }

    class Mammal1 : Animal1
    {
        public override string ToString()
        {
            return "Mammal";
        }

        public override string Print()
        {
            return "Mammal" + "->" + base.Print();
        }

    }

    class Bird1 : Animal1
    {
        public override string ToString()
        {
            return "Bird";
        }
        public override string Print()
        {
            return "Bird" + "->" + base.Print();
        }
    }

    class Falcon1 : Bird1 , IFlyable1
    {
        public override string ToString()
        {
            return "Falcon";
        }
        public override string Print()
        {
            return "Falcon" + "->" + base.Print();
        }

        public string CanFly()
        {
            return ", it can fly";
        }
      
    }

    class Penguin1 : Bird1
    {
        public override string ToString()
        {
            return "Penguin";
        }

        public override string Print()
        {
            return "Penguin" + "->" + base.Print();
        }
    }

    class Cat1 : Mammal1
    {
        public override string ToString()
        {
            return "Cat";
        }

        public override string Print()
        {
            return "Cat" + "->" + base.Print();
        }
    }

    class Bat1 : Mammal1 , IFlyable1
    {
        public override string ToString()
        {
            return "Bat";
        }

        public override string Print()
        {
            return "Bat" + "->" + base.Print();
        }

        public string CanFly()
        {
            return ", it can fly";
        }
    }

    class Plane : IFlyable1
    {
        public string CanFly()
        {
            return ", it can fly";
        }
    }

    class Zoo
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Animal1[] animals = new Animal1[20];

            Falcon1 f;


            for (int i = 0; i < animals.Length; i++)
            {
                switch (rand.Next(7))
                {
                    case 0:
                        animals[i] = new Cat1(); break;
                    case 1:
                        animals[i] = new Bat1(); break;
                    case 2:
                        animals[i] = new Penguin1(); break;
                    case 3:
                        animals[i] = new Falcon1(); break;
                    case 4:
                        animals[i] = new Bird1(); break;
                    case 5:
                        animals[i] = new Mammal1(); break;
                    default:
                        animals[i] = new Animal1(); break;

                }


                Console.Write("ToString() : {0,10} , Print() : {1,-30} ", animals[i], animals[i].Print());

                //if (animals[i] is Bat1)
                //    Console.WriteLine(((Bat1)animals[i]).CanFly());
                //else if (animals[i] is Falcon1)
                //{
                //    f = (Falcon1)animals[i];
                //    Console.WriteLine(f.CanFly());
                //}
                //else
                //    Console.WriteLine();

                //IFlyable1 fd = (IFlyable1)animals[i];


                if (animals[i] is IFlyable1)
                    Console.WriteLine(((IFlyable1)animals[i]).CanFly());
                else
                    Console.WriteLine();


            }

            Console.ReadKey();

        }
    }
}
