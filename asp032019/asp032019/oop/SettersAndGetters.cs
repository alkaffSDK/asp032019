﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop { 
     class User
    {
        public string Name;
        protected int Age; 
        public string Email;
        protected string Password;

        public bool Login(string email, string pass)
        {
            return Email.Equals(email, StringComparison.OrdinalIgnoreCase) && Password.Equals(pass);
        }

        public override string ToString()
        {
            return string.Format("User [\'{0}\',{1},\'{2}\']", Name,Age,Email);
        }

        public void SetAge(int a)
        {
            if (a > 0 && a < 150)
                Age = a;
            
        }

        public int GetAge() { return Age; }
    }

    class SettersAndGetters
    {

        static void Main(string[] args)
        {
            User u = new User();
            u.Name = "Ahmed";
           // u.Age = -36;
            u.SetAge(36);
            u.Email = "a.alkaff@sdkjordan.com";

            Console.WriteLine("Your age :"+u.GetAge());

            Console.WriteLine(u);

            Console.ReadKey();
        }
    }
}
