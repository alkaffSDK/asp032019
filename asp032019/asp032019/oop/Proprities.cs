﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{
    class User
    {
        private string UserName;
        private string FullName;
        private string Password;
        private bool? Gender = null;
        private int _Age;
        public int Age
        {
            set
            {
                if (value > 0 && value < 150)
                    _Age = value;
            }
            get { return _Age; }
        }

        public bool SetUserName(string un)
        {
            if (IsValidUserName(un))
            {
                UserName = un;
                return true;
            }
            return false;
        }
        public string GetUserName() { return UserName; }
        public bool SetFullName(string fn)
        {
            if (IsValidFullName(fn))
            {
                FullName = fn;
                return true;
            }
            return false;
        }
        public bool SetFullName(string fn, string sn, string ln)
        {
            string name = fn + " " + sn + " " + ln;
            return SetFullName(name);
        }
        public string GetFullName() { return FullName; }

        public bool SetPassword(string pass)
        {
            if (IsValidPassword(pass))
            {
                Password = pass;
                return true;
            }
            return false;
        }

        private bool IsValidPassword(string pass)
        {
            if (string.IsNullOrEmpty(pass) ||
                string.IsNullOrWhiteSpace(pass) ||
                pass.Length < 7)
                return false;

            bool HasDigit = false;
            bool HasUpper = false;
            bool HasLower = false;
            int letters = 0;

            for (int i = 0; i < pass.Length; i++)
            {
                if (!HasDigit && char.IsDigit(pass[i]))
                    HasDigit = true;
                else if (!HasUpper && char.IsUpper(pass[i]))
                    HasUpper = true;
                else if (!HasLower && char.IsLower(pass[i]))
                    HasLower = true;

                if (char.IsLetterOrDigit(pass[i]))
                    letters++;
            }

            return (HasDigit && HasUpper && HasLower && letters >= 6);
        }

        public bool checkPassword(string pass) { return Password.Equals(pass); }

        public bool SetGender(string gen)
        {
            if (gen.ToLower().Equals("male") || gen.ToLower().Equals("m"))
            {
                Gender = true;
                return true;
            }
            if (gen.Equals("female", StringComparison.CurrentCultureIgnoreCase) ||
                gen.Equals("f", StringComparison.CurrentCultureIgnoreCase))
            {
                Gender = false;
                return true;
            }
            return false;
        }
        public void SetGender(bool g)
        {
            Gender = g;
        }
        public bool? GetGender() { return Gender; }
        public string GetGenderName()
        {
            if (Gender == null)
                return "Not specified";
            else if (Gender == true)
                return "Male";
            else
                return "Female";
        }

        //public bool SetAge(int a)
        //{
        //    if (a > 0 && a < 150)
        //    {
        //        Age = a;
        //        return true;
        //    }
        //    return false;
        //}

        //public int GetAge() { return Age; }
        private bool IsValidFullName(string fn)
        {
            if (string.IsNullOrEmpty(fn) || string.IsNullOrWhiteSpace(fn) || fn.Length < 7)
                return false;

            bool HasSpace = false;
            int Letters = 0;

            // Loop to all the charters of fn 
            for (int i = 0; i < fn.Length; i++)
            {
                if (char.IsDigit(fn[i]))
                    return false;

                if (char.IsLetter(fn[i]))
                    Letters++;

                if (char.IsWhiteSpace(fn[i]))
                    HasSpace = true;
            }

            if (Letters < 6)
                return false;

            if (!HasSpace)
                return false;

            return true;
        }

        private bool IsValidUserName(string un)
        {
            return !string.IsNullOrEmpty(un) &&
                !string.IsNullOrWhiteSpace(un) &&
                un.Length >= 6;
        }

        // Proprity
        public int ANY;
        private int _AgeProp;
        public int AgeProp
        {
            get
            {
                return _AgeProp;     // get 
            }
            private set
            {
                if (value > 0 && value < 150)
                    _AgeProp = value;
            }
        }
    }

    // class User1 is the same as class User but with using the proporities as needed
    class User1
    {
        private string Password;
        private bool? Gender = null;

        private int _Age;
        public int Age
        {
            set
            {
                if (value > 0 && value < 150)
                    _Age = value;
            }
            get { return _Age; }
        }
        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set
            {
                if (IsValidUserName(value))
                    _UserName = value;
            }
        }
        private string _FullName;
        public string FullName {
            get { return _FullName; }
            set
            {
                if (IsValidFullName(value))
                    _FullName = value;
            }
        }
      
        public void SetFullName(string fn, string sn, string ln)
        {
            string name = fn + " " + sn + " " + ln;
             FullName = name;
        }
        public bool SetPassword(string pass)
        {
            if (IsValidPassword(pass))
            {
                Password = pass;
                return true;
            }
            return false;
        }

        private bool IsValidPassword(string pass)
        {
            if (string.IsNullOrEmpty(pass) ||
                string.IsNullOrWhiteSpace(pass) ||
                pass.Length < 7)
                return false;

            bool HasDigit = false;
            bool HasUpper = false;
            bool HasLower = false;
            int letters = 0;

            for (int i = 0; i < pass.Length; i++)
            {
                if (!HasDigit && char.IsDigit(pass[i]))
                    HasDigit = true;
                else if (!HasUpper && char.IsUpper(pass[i]))
                    HasUpper = true;
                else if (!HasLower && char.IsLower(pass[i]))
                    HasLower = true;

                if (char.IsLetterOrDigit(pass[i]))
                    letters++;
            }

            return (HasDigit && HasUpper && HasLower && letters >= 6);
        }

        public bool checkPassword(string pass) { return Password.Equals(pass); }

        public bool SetGender(string gen)
        {
            if (gen.ToLower().Equals("male") || gen.ToLower().Equals("m"))
            {
                Gender = true;
                return true;
            }
            if (gen.Equals("female", StringComparison.CurrentCultureIgnoreCase) ||
                gen.Equals("f", StringComparison.CurrentCultureIgnoreCase))
            {
                Gender = false;
                return true;
            }
            return false;
        }
        public void SetGender(bool g)
        {
            Gender = g;
        }
        public bool? GetGender() { return Gender; }
        public string GetGenderName()
        {
            if (Gender == null)
                return "Not specified";
            else if (Gender == true)
                return "Male";
            else
                return "Female";
        }
        private bool IsValidFullName(string fn)
        {
            if (string.IsNullOrEmpty(fn) || string.IsNullOrWhiteSpace(fn) || fn.Length < 7)
                return false;

            bool HasSpace = false;
            int Letters = 0;

            // Loop to all the charters of fn 
            for (int i = 0; i < fn.Length; i++)
            {
                if (char.IsDigit(fn[i]))
                    return false;

                if (char.IsLetter(fn[i]))
                    Letters++;

                if (char.IsWhiteSpace(fn[i]))
                    HasSpace = true;
            }

            if (Letters < 6)
                return false;

            if (!HasSpace)
                return false;

            return true;
        }

        private bool IsValidUserName(string un)
        {
            return !string.IsNullOrEmpty(un) &&
                !string.IsNullOrWhiteSpace(un) &&
                un.Length >= 6;
        }

    }

    class Proprities
    {
        static void Main(string[] args)
        {
            int[] arr = new int[4];
           
            User user = new User();
            //user.AgeProp = 10;
            int c = user.AgeProp;
            user.Age = 205;//user.SetAge(25);
           // user.ANY = 25;
            //user.AgeProp = 25;

            //user.AgeProp = -1;            // will call set 
            //Console.WriteLine(user.AgeProp);    // will call get 

            //Console.WriteLine(user.ANY);
            user.SetFullName("Ahmed", "Ali", "Mohammed");
            if (user.SetPassword("1234abcd"))
                Console.WriteLine("Password has been changed");
            else
                Console.WriteLine("No change in the password !");

            Console.WriteLine("Full name:{0}", user.GetFullName());
            Console.WriteLine("Age :{0}", user.Age);



            Console.ReadKey();
        }
    }
}
