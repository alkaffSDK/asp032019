﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop
{

    interface IFlyable
    { 
        string CanFly();

    }
    class Creature
    {
        //// to count the number of objects created from this class
        //public static int Counter = 0;
        //public int AnotherCounter = 0;
        //public int ID { get; set; }
        //public Creature()
        //{
        //    AnotherCounter++;
        //    Mansaf obj = new Mansaf();
        //    obj.meat = "in object";

        //    Mansaf.someThing = 10;

        //    Counter++;
        //    //Console.WriteLine("Creature()");
        //}


        public override string ToString()
        {
            return "Creature";
        }

        public virtual string PrintMyType()
        {
            return "Creature";
        }
    }
    class Animal : Creature
    {
        //private static int _id = 0;

        //public int ID { get; }
        //public static int Counter = 0;
        //public int AnimalCode { get; set; }
        //public Animal()
        //{
        //    ID = ++_id;
        //    AnotherCounter++;
        //    Counter++;
        //    //Console.WriteLine("Animal()");
        //}

        public override string PrintMyType()
        {
            return "Animal";
        }


        public override string ToString()
        {
            return "Animal";
        }
    }

    class Bird : Animal
    {
        //public static int Counter = 0;
        //private int BirdNumber { get; set; }
        //public Bird()
        //{
        //    AnotherCounter++;
        //    Counter++;
        //    //Console.WriteLine("Bird()");
        //}
        public override string PrintMyType() { return "Bird"; }
        public override string ToString()
        {
            return "Bird";
        }
    }

    class Mammal : Animal
    {
        //public static int Counter = 0;
        //public Mammal()
        //{
        //    AnotherCounter++;
        //    Counter++;
        //    //Console.WriteLine("Mammal()");
        //}
        public override string PrintMyType() { return "Mammal"; }

        public override string ToString()
        {
            return "Mammal";
        }
    }

    class Penguin : Bird
    {
        //public static int Counter = 0;
        //public Penguin()
        //{
        //    AnotherCounter++;
        //    Counter++;
        //    //Console.WriteLine("Penguin()");
        //}
        public override string PrintMyType() { return "Penguin"; }
        public override string ToString()
        {
            return "Penguin";
        }
    }

    class Falcon : Bird , IFlyable
    {
        //public static int Counter = 0;
        //public Falcon()
        //{
        //    AnotherCounter++;
        //    Counter++;
        //    // Console.WriteLine("Falcon()");
        //}

        public string CanFly() { return ", it can fly."; }

        public override string PrintMyType() { return "Falcon"; }
        //public string CanFly() { return ", it can fly"; }

        public override string ToString()
        {
            return "Falcon";
        }
    }

    class Bat : Mammal, IFlyable
    {
        //public static int Counter = 0;
        //public Bat()
        //{
        //    AnotherCounter++;
        //    Counter++;
        //    // Console.WriteLine("Bat()");
        //}


        public  string CanFly() { return ", it can fly."; }

        public override string PrintMyType() { return "Bat"; }
        //public string CanFly() { return ", it can fly"; }

        public override string ToString()
        {
            return "Bat";
        }
    }

    class Cat : Mammal
    {
        //public static int Counter = 0;
        //public Cat()
        //{
        //    AnotherCounter++;
        //    Counter++;
        //    // Console.WriteLine("Cat()");
        //}
        public override string PrintMyType() { return "Cat"; }

        public override string ToString()
        {
            return "Cat";
        }
    }
    class Eagle : Bird , IFlyable
    {

        public override string PrintMyType() { return "Eagle"; }

        public override string ToString()
        {
            return "Eagle";
        }
        public string CanFly() { return ", it can fly."; }
    }

    class PloymorphisimDemo2
    {
       
        static void Main(string[] args)
        {
            Random random = new Random();


            Creature[] animals = new Creature[30];

            for (int i = 0; i < animals.Length; i++)
            {
                switch(random.Next(9))
                {
                    case 0:
                        animals[i] = new Penguin(); break;
                    case 1:
                        animals[i] = new Falcon(); break;
                    case 2:
                        animals[i] = new Bat(); break;
                    case 3:
                        animals[i] = new Cat(); break;
                    case 4:
                        animals[i] = new Mammal(); break;
                    case 5:
                        animals[i] = new Bird(); break;
                    case 6:
                        animals[i] = new Animal(); break;
                    case 7:
                        animals[i] = new Eagle(); break;
                    default:
                        animals[i] = new Creature(); break;
                }

                //Console.WriteLine("{0,-3}) {1,30} :{2}",i,animals[i].ToString(),animals[i].PrintMyType());
                Console.Write("{0,-3}) {1}", i, animals[i]);
                // Casting Exception:  Console.WriteLine(((Bat)animals[i]).CanFly());


                // Wihtout interface 
                // if the object refred by the variable animals[i] is of type Bat
                //if (animals[i] is Bat)
                //    Console.WriteLine(((Bat)animals[i]).CanFly());
                //else if (animals[i] is Falcon)
                //{
                //    Falcon f = (Falcon)animals[i];
                //    Console.WriteLine(f.CanFly());
                //}else
                //    Console.WriteLine();


                // using interface 
                if (animals[i] is IFlyable)
                    Console.WriteLine(((IFlyable)animals[i]).CanFly());
                else
                    Console.WriteLine();

            }

            Console.ReadKey();

        }
    }
}
