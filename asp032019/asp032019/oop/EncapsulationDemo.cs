﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop
{
    class A
    {
        public int Public;
        protected int Protected;
        private int Private;


        public void method()
        {
            Private = 10;
            Protected = 10;
            Public = 10;
        }
    }

   
    class EncapsulationDemo
    {
        static void Main(string[] args)
        {
            A obj = new A();
            //obj.a1 = 10;        // 
            //obj.m1 = 10;        // 

            obj.Public = 10;

            
        }
    }
}
