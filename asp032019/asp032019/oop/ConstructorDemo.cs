﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{
    /// <summary>
    /// Constructor : is a special method with the same name as the class and no return data type
    /// Any class must have a constructor , if the developer dose not create one, 
    /// then the compliler will create a default empty contstructor 
    /// Constructor can be overloaded 
    /// A Constructor must be called after creating any object of the class
    /// 
    /// Overloading : The process of defining more than one method using the same name, but diffrenct parameter list
    /// 
    /// Overridding : 
    /// </summary>

    class Human
    {
        public void Human1()
        {
            // this is a method
        }
        //public Human()
        //{
        //    // this is an empty contrcutor 
        //}
        public Human() 
        {
            Console.WriteLine("Human()");
        }
        public Human(int a):this()
        {
            Console.WriteLine("Human(int)");
        }

    }

    class SubHuman :Human
    {
        public SubHuman():this(0)
        {

        }
        public SubHuman(int a) :base(a)
        {

        }

    }

    class OverloadingDemo
    {
        public int Add()
        {
            return 0;
        }
        public int Add(int a)
        {
            return a;
        }
        public int Add(int a, int b )
        {
            return a + b ;
        }
        public double Add(double a, double b)
        {
            return a + b;
        }
        public double Add(int a, double b)
        {
            return a + b;
        }
        public double Add(double a, int b)
        {
            return a + b;
        }
    }
    class ConstructorDemo
    {
        static void Main(string[] args)
        {
            Human h = new Human(10);


            Console.ReadKey();

        }
    }
}
