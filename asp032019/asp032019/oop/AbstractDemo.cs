﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{
    // abstract method : is a method without a body, but it can be exist only in a abstract class 
    // abstract class : is the class that may containt an abstract methods
    // can NOT create an object of abstract class dirctly using new operator
    // abstract methods can't be private or static
    // non virtual or abstract	 methods can NOT be overridden
    // virtual methods  can be overridden
    // abstract methods must be overriden

    abstract class Shape
    {
        // abstract class can have data members or non abstract methods 
        int a; 
        public Shape()
        {
            Console.WriteLine("Shape()");
        }
        /// <summary>
        /// 
        /// </summary>
        public void Method()
        {
        }
        public abstract void AbstractMethod();
        public abstract double Area();
    }

    class Rectangle : Shape
    {
        /// <summary>
        /// Width of the rectangle
        /// </summary>
        public int Width  { get; set; }
        public int Height { get; set; }
        /// <summary>
        /// This is the constructor of the rectangle class
        /// </summary>
        /// <param name="w">The width value</param>
        /// <param name="h">The hight value</param>
        public Rectangle(int w, int h)
        {
            Width  = w;
            Height = h;
        }
        public override void AbstractMethod()
        {
            Console.WriteLine("AbstractMethod()");
        }
        public override double Area()
        {
            return Width * Height;
        }

        public override string ToString()
        {
            return string.Format("{0,-10} [ {1}:{2} , {3}:{4}]", "Rectangle", "Width", Width, "Height", Height);
        }
    }
     class Squar : Rectangle
    {
        public Squar(int s):base(s,s)
        {}
        public override string ToString()
        {
            return string.Format("{0,-10} [ {1}:{2}]", "Squar", "Side", Width);
        }
    }
    class Triangle : Shape
    {
        public Triangle(int b, int h)
        {
            Base = b;
            Height = h; 
        }
        public int Base { get; set; }
        public int Height { get; set; }
        public override void AbstractMethod()
        {
            Console.WriteLine("");
        }
        public override double Area()
        {
            return 0.5 * Base * Height;
        }
        public override string ToString()
        {
            return string.Format("{0,-10} [ {1}:{2} , {3}:{4}]", "Triangle", "Base", Base, "Height", Height);
        }
    }
    class AbstractDemo
    {
        static void Main(string[] args)
        {
            // Can't create an instance of an abstract class direclty using new operator
            // becuase you can execute the abstract methods 
            //Shape shape = new Shape();
            //shape.AbstractMethod();
            Rectangle r = new Rectangle(15,5);
            r.AbstractMethod();

            Shape[] shaps = new Shape[3];
            shaps[0] = new Rectangle(10,5);
            new Rectangle(15, 2);
            shaps[1] = new Squar(4);
            shaps[2] = new Triangle(5,8);
            for (int i = 0; i < shaps.Length; i++)
                Console.WriteLine("The area for the shape \"{0}\" is :{1}",shaps[i],shaps[i].Area());
       

            Console.ReadKey();
        }
    }
}
