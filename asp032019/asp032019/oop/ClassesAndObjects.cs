﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP012019
{

    class Person
    {
        // to have 
        public string Name;
        public int age;
        public Person Father, Mother;
        public Person[] childern;

        // to do 
        public void moveForward()
        {

        }

    }

    class Student : Person { }

    class ClassesAndObjects
    {

        static void Main(string[] args)
        {
            // Class : is a description/blueprint/template for a type, 
            //          that defnes it's states (variables and objects) and behaviours (methods)

            // Object : is an instance of a class.  

            // Variable : a symbolic name paired s memory location refrence.

            // Object class : is a class called Object, this class is the parent of any class in C#

            int x = 10;                     // x is a variable of type int
            int[] arr = new int[2];         // arr is a variable of tyoe int[]

            Random r = new Random();        // r is a variable od type Random

            Person Pvar;                    // create a variable of type Person
            new Person();                   // create an object of type Person
            Person p =  new Person();       // variable p is refereing to object of type Person

            object o1 = new Random();
            object o2 = new Person();
            object o3 = new string(new char[] { 'a','b'});
            object o4 = 10;

            object obj = new object();  // obj is a variable of type Object that referce to an object of type Object 

            Person a = new Person();
            a.Name = "Adam";
            a.age = 950;
            a.Father = null;
            a.Mother = null;

            Person b = new Person();
            b.Father = a;

        }
    }

    class University
    {
        Colage[] colges = new Colage[10];
    }

    class Colage
    {

    }
}
