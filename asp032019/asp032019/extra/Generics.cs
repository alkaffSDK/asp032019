﻿using ASPNET022019.oop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.extra
{

    class Pair
    {
        // Key
        public object Key { set; get; }

        // Value
        public object Value { set; get; }
    }

    class Pair<K, V>         // Pair<int,string>
    {
        // Key
        public K Key { set; get; }

        // Value
        public V Value { set; get; }
    }


    class Generics
    {
      
      
        static void Main(string[] args)
        {

            Pair p = new Pair();
            p.Key = 1;
            p.Value = "String";


            Pair p1 = new Pair();
            p1.Key = "a";
            p1.Value = new Rectangle(5,5);



            Pair<int, string> p2 = new Pair<int,string>();
            p2.Key = 1;
            p2.Value = "String";

            

            Pair<string, Shape> p3 = new Pair<string, Shape>();
            p3.Key = "1";
            p3.Value = new Rectangle(5, 5);


            //Pair<char, Shape> p5 = CreatePair<char, Shape>();
            
            Pair<int,string> p4 = new Pair<int, string>();
            Pair<string, Shape> p6 = new Pair<string, Shape>();

            Console.WriteLine(Equals<int>(10,15));
            Console.WriteLine(Equals<double>(10.5, 15.5));
            Console.WriteLine(Equals<string>("10.5", "15.5"));
            Console.WriteLine(Equals<Shape>(new Rectangle(10,20), new Triangle(10,5)));

        }

        static bool Equals<K>(K a, K b)
        {
            return a.Equals(b);
        }

        //static Pair<K,V> CreatePair<K,V>()
        //{
        //    Pair<K, V> p = new Pair<K, V>();
        //    Random r = new Random();
        //    switch(r.Next(1,3))
        //    {
        //        case 0:
        //            p.Key = 1;
        //            p.Value = "String";
        //            break;
        //        case 1:
        //            p.Key = "string";
        //            p.Value = "String";
        //            break;
        //        case 2:
        //            p.Key = 'c';
        //            p.Value = new Rectangle(5,3);
        //            break;
        //    }

        //    return p;
        //}

        
    }
}
