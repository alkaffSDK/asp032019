﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.extra
{
    class User
    {
        private string UserName ;
        private int Age;
        public int AgeProp {
            set
            {
                if (value > 0 && value < 150)
                {
                    Age = value;            // set
                    return ;
                }
                throw new ArgumentOutOfRangeException("Invalid age value.");
            }
            get { return Age; }
        }

        public bool SetUserName(string un)
        {
            if (string.IsNullOrWhiteSpace(un))
            {
                UserName = un;
                return true;
            }
            throw new ArgumentException("Invalid user name.");

        }

        public string GetUserName() { return UserName; }

        public bool SetAge(int a)
        {
            AgeProp = 10;
            if (a >0 && a < 150 )
            {
                Age =  a;
                return true;
            }
            throw new ArgumentOutOfRangeException("Invalid age value.");

        }

        public int GetAge() { return Age; }

        public string MyProperty { get; set; }
    }
    class Properties
    {
        static void Main(string[] args)
        {
            User u = new User();
            u.SetAge(50);
            u.AgeProp = 50;
           


            Console.WriteLine(u.GetAge());
            Console.WriteLine(u.AgeProp);
        }
    }
}
