﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.extra
{
    class DynamicArray1<T>
    {

        private static int INITIAL_CAPACITY = 4;
        private T[] array;

        public int Size { get; private set; }

        public DynamicArray1(int capcity)
        {
            if(capcity > 0)
                array = new T[capcity];
            else
                array = new T[INITIAL_CAPACITY];

        }
        public DynamicArray1():this(INITIAL_CAPACITY)
        {}

        public void Add(T element)
        {
            if (array.Length == Size)
                Expand();

            array[Size++] = element;
        }

        public void Add(int index, T element)
        {
            if (index < 0 || index > Size)
                throw new IndexOutOfRangeException("Invalid index value:" + index);

            if (array.Length == Size)
                Expand(index, element);
            else
            {
                int j = Size;
                while (j > index)
                {
                    array[j] = array[j - 1];
                    j--;
                }

                array[index] = element;
                Size++;
            }
        }


        public override string ToString()
        {
            if (Size == 0)
                return "{ }";

            StringBuilder builder = new StringBuilder("{");
            for(int i=0;i<Size;i++)
            {
                builder.Append(array[i]);
                    if (i == Size - 1)
                        builder.Append("}");
                    else
                        builder.Append(",");
            }
               
            return builder.ToString();
        }


     

        private void Expand()
        {
            T[] temp = new T[array.Length * 2];
            for (int i = 0; i < array.Length; i++)
                temp[i] = array[i];

            array = temp;
        }

        private void Expand(int index, T element)
        {
            T[] temp = new T[array.Length * 2];
            for (int i = 0; i <= array.Length; i++)
            {
                if (i < index)
                    temp[i] = array[i];
                else if (i == index)
                    temp[i] = element;
                else
                    temp[i] = array[i-1];
            }
            array = temp;
            Size++;
        }


        // TODO: implement the following methods 

        public T Remove(int index)
        {
            // TODO: Remove the element at the specified index, and return it or return null otherwise
            // Throw exception if invalid index.
            throw new NotImplementedException();
        }

        public bool Remove(T element)
        {
            // TODO: Remove  the first occurence of the element with specified value,
            //and return true if it was removed or return false otherwise.
            throw new NotImplementedException();
        }

        public int RemoveAll(T element)
        {
            // TODO: Remove All  occurences of the element with specified value, 
            //and return the number of removed elements, or 0 if no element was removed.
            throw new NotImplementedException();
        }
        public T RemoveFirst()
        {
            // TODO: Remove the first element, 
            // and return it, or null if empty array.
            throw new NotImplementedException();
        }
        public T RemoveLast()
        {
            // TODO: Remove the last element, 
            //and return it, or null if empty array.
            throw new NotImplementedException();
        }
        public int Find(T element)
        {
            // TODO: Find the first occurence of the element, 
            // and return it's index, or -1 if not exist.
            throw new NotImplementedException();

        }
        public int[] FindAll(T element)
        {
            // TODO: Find the index of All occurence of the element, 
            // and return array of the index, or null if not exist.
            throw new NotImplementedException();
        }

        public bool Contains(T element)
        {
            // TODO: check if the array contains the specified element.
            // return true if exist or false otherwise.
            throw new NotImplementedException();
        }

        public void Repeat(T element, int count)
        {
            // TODO: add the element repeatidly  n number of times at the end .
            // Example : {1,2,3,4,5}    after Repeat(10,3) it will be {1,2,3,4,5,10,10,10}
            throw new NotImplementedException();
        }
        public void Repeat(T element, int offset, int count)
        {
            // TODO: add the element repeatidly  n number of times  strating from index offset.
            // Example : {1,2,3,4,5}    after Repeat(10,2,3) it will be {1,2,10,10,10,3,4,5}

            throw new NotImplementedException();
        }
    }


    class Program1
    {
        static void Main(string[] args)
        {
            DynamicArray<int> a = new DynamicArray<int>(8);     // 8 
            //DynamicArray<int> a1 = new DynamicArray<int>();     // 4
            //DynamicArray<int> a2 = new DynamicArray<int>(-8);    // 4


            Console.WriteLine(a);
            a.Add(55);
            a.Add(20);
            a.Add(17);
            a.Add(52);
            a.Add(7);
            a.Add(425);
            a.Add(87);
            a.Add(98);
            a.Add(63);
            
            Console.WriteLine(a);
            a.Add(2, 222);
            Console.WriteLine(a);
            a.Add(11, 00);
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }


}
