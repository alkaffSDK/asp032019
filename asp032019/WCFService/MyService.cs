﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MyService" in both code and config file together.
    public class MyService : IMyService
    {
        public string Hello()
        {
            return "Hello world!";
        }

        public string HelloByName(string name)
        {
            return "Hello "+name;
        }
    }
}
