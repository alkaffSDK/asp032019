﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowWCFConsumer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (ServiceReference1.MyServiceClient client = new ServiceReference1.MyServiceClient("BasicHttpBinding_IMyService"))
            {
                label1.Text =  client.HelloByName(textBox1.Text);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (ServiceReference1.MyServiceClient client = new ServiceReference1.MyServiceClient("NetTcpBinding_IMyService"))
            {
                label2.Text = client.HelloByName(textBox2.Text);
            }

        }
    }
}
